const PKG = require('./package.json');

module.exports = {
  name: PKG.name.split('/').pop(),

  alias: {
    // Utils
    '@helpers': './src/helpers',
    '@mocks': './__mocks__',
    '@package': './src',
    '@storyteller': './__storyteller__',
    '@fonts': '@soyjavi/reactor-fonts/dist',
    // React Components
    '@atoms': './src/components/atoms',
    '@contexts': './src/contexts',
    '@hooks': './src/hooks',
    '@molecules': './src/components/molecules',
    '@primitives': './src/components/primitives',
    // Theming
    '@theming': './src/theming',
  },

  development: {
    devServer: {
      /**
       * Configuration Object for devServer, see https://v4.webpack.js.org/configuration/dev-server/ for reference
       */
      proxy: {
        '/api/*': {
          secure: false,
          changeOrigin: true,
          target: 'https://soyjavi.com/api',
        },
      },
    },
  },
};
