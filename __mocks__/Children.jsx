import React from 'react';

import { COLOR, SIZE } from '@hooks';
import { Text, View } from '@primitives';

const Children = ({ ...inherit }) => (
  <View backgroundColor={COLOR.ACCENT} padding={SIZE.M} {...inherit}>
    <Text small>CHILDREN</Text>
  </View>
);

Children.displayName = 'Children';

export { Children };
