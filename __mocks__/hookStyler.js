import { StyleSheet } from 'react-native';

import { ALIGN, SPACE } from '@hooks';

export const styles = StyleSheet.create({
  test: {
    minWidth: 10,
  },
});

export const hookStyler = {
  // useStyler
  margin: [SPACE.S, SPACE.M, SPACE.L, SPACE.XL],
  paddingHorizontal: SPACE.XS,
  paddingBottom: SPACE.XL,
  alignSelf: ALIGN.CENTER,
  style: [styles.test, { backgroundColor: '#fff' }],
  // useSupportedProps
  id: '1980',
};
