const IMAGE_URL = 'https://via.placeholder.com/256/?text=image';
const IMAGE_2x_URL = 'https://via.placeholder.com/512/?text=image';

export { IMAGE_URL, IMAGE_2x_URL };
