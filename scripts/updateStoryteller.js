/* eslint-disable no-console */
const glob = require('glob');

const fs = require('fs');

const { line } = require('./command');
const FOLDER = '__storyteller__';
const STORYTELLER_INDEX = `./${FOLDER}/index.js`;

const updateStoryteller = () => {
  const chapters = glob.sync('**/*.chapter.*sx', { ignore: ['node_modules/**'] });
  let imports = '';

  if (chapters && chapters.length) {
    console.log(line(), `⚙️  Building storyteller...`, line());

    chapters.forEach((chapter) => {
      const file = chapter.includes(FOLDER) ? chapter.replace(FOLDER, '.') : `../${chapter}`;

      imports += `export * from '${file}';\n`;
    });
  }

  if (imports.length > 0) {
    try {
      fs.writeFileSync(STORYTELLER_INDEX, imports);
    } catch (error) {
      console.log(error);
    }
  }
};

module.exports = { updateStoryteller };
