const { name, standalone } = require('../.soyjavirc.js');
const { breakline, command } = require('./command');

(() => {
  if (!standalone) command('yarn version --patch');

  if (standalone) {
    // TODO: We should make a correct pipeline

    const input = `${__dirname}/../build-standalone`;
    const assets = `${input}/assets`;
    const output = ` ${input}/${name}`;
    const verbose = false;

    command(`rm -rf ${input}/index.html`, verbose);
    command(`mkdir ${output}`, verbose);
    command(`mv ${assets}/index*.js ${output}/index.js`, verbose);
    // TODO: We will not have .css
    // command(`mv ${assets}/index*.css ${output}/index.css`, verbose);
    command(`rm -rf ${assets}`, verbose);
  }

  breakline();
})();
