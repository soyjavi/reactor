const { babel } = require('./babel');
const { breakline, command } = require('./command');
const { updateStoryteller } = require('./updateStoryteller');

(() => {
  const package = './node_modules/@soyjavi/reactor';
  const params = process.argv.splice(2).join(' ');

  const execute = ({ update } = {}) => {
    babel();

    breakline();

    if (update) updateStoryteller();

    // * Forcing storyteller to use the development version of aurora ----------
    command(`rm -rf ${package}`);
    command(`mkdir ${package}`);
    command(`mkdir ${package}/build`);
    command(`cp -R package.json build ${package}`);
    command(`cp -R build/** ${package}/build`);
    // * -----------------------------------------------------------------------

    command(`expo ${params}`);

    breakline();
  };

  execute({ update: !params.includes('build:web --no-pwa') });
})();
