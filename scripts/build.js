const { standalone } = require('../.soyjavirc.js');
const { babel } = require('./babel');
const { breakline, command } = require('./command');

(() => {
  const params = process.argv.splice(2).join(' ');

  command('yarn install', false);

  // ! If you want ES6 friendly build ------------------------------------------
  // babel(params, 'babel.config.production.js');
  // ! -------------------------------------------------------------------------

  babel(params);

  command(
    'npx -p typescript tsc build/**/*.js --declaration --allowJs --emitDeclarationOnly --skipLibCheck --outDir build',
  );

  breakline();
})();
