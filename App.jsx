import { Chapter, serializeName, Storyteller } from '@package/storybook';
import React from 'react';

import * as chapters from '@storyteller';

import PKG from './package.json';

const { repository: { url } = {}, version } = PKG;

const App = () => (
  <Storyteller title="Reactor" version={version}>
    {Object.values(chapters).map((chapter, index) => {
      const { domain = '', name: component = '' } = serializeName(chapter.displayName);
      const base = domain.split(' ')[1] || '';

      return React.createElement(
        Chapter,
        {
          doc: `${url}/blob/master/__storyteller__/${base.toLowerCase()}/${component}.chapter.jsx`,
          jsx: false,
          key: `chapter:${index}`,
          title: chapter.displayName,
        },
        React.createElement(chapter),
      );
    })}
    {/* <BoxChapter title="Box" /> */}
  </Storyteller>
);

export default App;
