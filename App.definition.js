const FONTS = {
  'font-default': require('@fonts/Circular-SP-Book.ttf'),
  'font-bold': require('@fonts/Circular-SP-Bold.ttf'),
  'reactor-icons': require('@fonts/icons.ttf'),
};

export { FONTS };
