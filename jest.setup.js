import './__mocks__/matchmedia.mock';

import { toMatchImageSnapshot } from 'jest-image-snapshot';
import StyleSheet from 'react-native-extended-stylesheet';

import { DefaultTheme } from '@theming';

StyleSheet.build(DefaultTheme);

expect.extend({ toMatchImageSnapshot });
