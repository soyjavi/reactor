import StyleSheet from 'react-native-extended-stylesheet';

import { DefaultTheme } from './theme.default';

const THEME_NAME = '$theme';
const SHORTCUT = '$';

const getStyleSheet = (key) => {
  try {
    return StyleSheet.value(key);
  } catch (error) {
    return undefined;
  }
};

export const Theme = {
  get: (key) => {
    let value;

    if (key) {
      value = getStyleSheet(`${SHORTCUT}${key}`);
    } else {
      value = {};
      Object.keys(DefaultTheme).forEach((key) => (value[key.replace(SHORTCUT, '')] = getStyleSheet(key)));
    }

    return value;
  },

  set: (theme = {}) => {
    StyleSheet.build({
      $theme: theme[THEME_NAME] || DefaultTheme[THEME_NAME],
      ...DefaultTheme,
      ...theme,
    });
  },
};
