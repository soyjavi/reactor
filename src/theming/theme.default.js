import { Easing } from 'react-native';

const FONT_HEADLINE_1 = 48;
const FONT_HEADLINE_2 = 34;
const FONT_HEADLINE_3 = 28;
const FONT_HEADLINE_4 = 22;
const FONT_PARAGRAPH = 18;
const FONT_ACTION = 14;
const FONT_SMALL = 13;
const LINE_HEIGHT = 1.2;

export const DefaultTheme = {
  $theme: 'reactor-v3',
  /* BORDER */
  $borderSize: 1,
  $borderStyle: 'solid',

  /* BUTTON */
  $buttonRadius: 0,
  $buttonColor: '#f00',

  /* COLOR PALLETE */
  $colorBase: '#ffffff',
  $colorContent: '#000000',
  $colorLighten: '#999999',
  $colorPrimary: '#755EFC',
  $colorSecondary: '#FFD337',
  $colorDisabled: '#F2F2F2',

  $colorError: '#F34E4E',
  $colorWarning: '#F5C975',
  $colorSuccess: '#49D09C',

  $colorOverlay: 'rgba(231, 231, 231, 0.8)',
  $colorTouchable: 'rgba(255, 255, 255, 0.15)',

  /* TYPOGRAPHY */
  $fontMap: {
    Headline: 'font-bold',
    Paragraph: 'font-default',
    Action: 'font-bold',
    Small: 'font-default',
  },

  $fontHeadline: 'Headline',
  $fontHeadlineStyle1: 'normal',
  $fontHeadlineVariant1: ['normal'],
  $fontHeadlineWeight1: '800',
  $fontHeadlineSize1: FONT_HEADLINE_1,
  $fontHeadlineHeight1: parseInt(FONT_HEADLINE_1 * LINE_HEIGHT),
  $fontHeadlinePaddingTop1: 0,
  $fontHeadlinePaddingRight1: 0,
  $fontHeadlinePaddingBottom1: 0,
  $fontHeadlinePaddingLeft1: 0,
  $fontHeadlineLetterSpacing1: 0,

  $fontHeadlineStyle2: 'normal',
  $fontHeadlineVariant2: ['normal'],
  $fontHeadlineWeight2: '800',
  $fontHeadlineSize2: FONT_HEADLINE_2,
  $fontHeadlineHeight2: parseInt(FONT_HEADLINE_2 * LINE_HEIGHT),
  $fontHeadlinePaddingTop2: 0,
  $fontHeadlinePaddingRight2: 0,
  $fontHeadlinePaddingBottom2: 0,
  $fontHeadlinePaddingLeft2: 0,
  $fontHeadlineLetterSpacing2: 0,

  $fontHeadlineStyle3: 'normal',
  $fontHeadlineVariant3: ['normal'],
  $fontHeadlineWeight3: '800',
  $fontHeadlineSize3: FONT_HEADLINE_3,
  $fontHeadlineHeight3: parseInt(FONT_HEADLINE_3 * LINE_HEIGHT),
  $fontHeadlinePaddingTop3: 0,
  $fontHeadlinePaddingRight3: 0,
  $fontHeadlinePaddingBottom3: 0,
  $fontHeadlinePaddingLeft3: 0,
  $fontHeadlineLetterSpacing3: 0,

  $fontHeadlineStyle4: 'normal',
  $fontHeadlineVariant4: ['normal'],
  $fontHeadlineWeight4: '800',
  $fontHeadlineSize4: FONT_HEADLINE_4,
  $fontHeadlineHeight4: parseInt(FONT_HEADLINE_4 * LINE_HEIGHT),
  $fontHeadlinePaddingTop4: 0,
  $fontHeadlinePaddingRight4: 0,
  $fontHeadlinePaddingBottom4: 0,
  $fontHeadlinePaddingLeft4: 0,
  $fontHeadlineLetterSpacing4: 0,

  $fontParagraph: 'Paragraph',
  $fontParagraphStyle: 'normal',
  $fontParagraphVariant: ['normal'],
  $fontParagraphWeight: '300',
  $fontParagraphSize: FONT_PARAGRAPH,
  $fontParagraphHeight: parseInt(FONT_PARAGRAPH * LINE_HEIGHT),
  $fontParagraphPaddingTop: 0,
  $fontParagraphPaddingRight: 0,
  $fontParagraphPaddingBottom: 0,
  $fontParagraphPaddingLeft: 0,
  $fontParagraphLetterSpacing: 0,

  $fontAction: 'Action',
  $fontActionStyle: 'normal',
  $fontActionVariant: ['normal'],
  $fontActionWeight: '800',
  $fontActionSize: FONT_ACTION,
  $fontActionHeight: parseInt(FONT_ACTION * LINE_HEIGHT),
  $fontActionPaddingTop: 0,
  $fontActionPaddingRight: 0,
  $fontActionPaddingBottom: 0,
  $fontActionPaddingLeft: 0,
  $fontActionLetterSpacing: 0,

  $fontSmall: 'Small',
  $fontSmallStyle: 'normal',
  $fontSmallVariant: ['normal'],
  $fontSmallWeight: '300',
  $fontSmallSize: FONT_SMALL,
  $fontSmallHeight: parseInt(FONT_SMALL * LINE_HEIGHT),
  $fontSmallPaddingTop: 0,
  $fontSmallPaddingRight: 0,
  $fontSmallPaddingBottom: 0,
  $fontSmallPaddingLeft: 0,
  $fontSmallLetterSpacing: 0,

  $fontInput: 'Paragraph',
  $fontInputStyle: 'normal',
  $fontInputVariant: ['normal'],
  $fontInputWeight: '300',
  $fontInputSize: FONT_PARAGRAPH,
  $fontInputHeight: FONT_PARAGRAPH * LINE_HEIGHT,
  $fontInputPaddingTop: 0,
  $fontInputPaddingRight: 16,
  $fontInputPaddingBottom: 0,
  $fontInputPaddingLeft: 16,
  $fontInputLetterSpacing: 0,

  /* ELEVATION */
  $elevationOffset: { width: 0, height: 8 },
  $elevationRadius: 18,
  $elevationOpacityS: 0.075,
  $elevationOpacityM: 0.15,
  $elevationColor: 'rgb(0, 0, 0)',
  $elevationLayerXS: 1,
  $elevationLayerS: 2,
  $elevationLayerM: 3,
  $elevationLayerL: 4,
  $elevationLayerXL: 5,

  /* ICON */
  $iconFamily: 'reactor-icons',
  $iconGlyphs: require('@fonts/icons.json'),
  $iconSize: 24,

  /* INPUT */
  $inputOptionSize: 20,
  $inputSize: 54,
  $inputSizeSmall: 40,
  $inputSizeLarge: 72,

  /* LAYOUT */
  $layoutMaxWidth: 1280,

  /* MOTION */
  $motionCollapse: 250,
  $motionExpand: 300,
  $motionSkeleton: 1200,
  $motionSlow: 5000,
  $motionEasing: {
    linear: () => Easing.linear(),
    spring: () => Easing.bezier(0.47, 1.64, 0.41, 0.8),
    quad: () => Easing.inOut(Easing.quad),
  },

  /* SPACING */
  $spaceXS: 8,
  $spaceS: 12,
  $spaceM: 16,
  $spaceL: 24,
  $spaceXL: 32,
  $spaceXXL: 56,
};
