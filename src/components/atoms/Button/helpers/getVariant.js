import { COLOR } from '@hooks';

import { BUTTON_VARIANT } from '../Button.definition';

export const getVariant = (variant, others = {}) => {
  switch (variant) {
    case BUTTON_VARIANT.PRIMARY:
      return {
        color: COLOR.PRIMARY,
      };

    case BUTTON_VARIANT.SECONDARY:
      return {
        color: COLOR.SECONDARY,
        outlined: true,
      };

    case BUTTON_VARIANT.OUTLINED:
      return {
        outlined: true,
      };

    // ! Will be deprecated in v4 ----------------------------------------------
    default: {
      const { color, outlined, rounded, squared } = others;

      return { color, outlined, rounded, squared };
    }
    // !------------------------------------------------------------------------
  }
};
