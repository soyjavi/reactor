import PropTypes from 'prop-types';
import React, { useState } from 'react';

import { ALIGN, COLOR, POINTER, SPACE, styles } from '@hooks';
import { Icon, Motion, MOTION_TYPES, Text, Touchable } from '@primitives';
import { Theme } from '@theming';

import { BUTTON_VARIANT } from './Button.definition';
import { style } from './Button.style';
import { getVariant } from './helpers';

const Button = ({
  busy,
  children,
  disabled,
  icon,
  large,
  small,
  upperCase = true,
  variant,
  wide = false,
  onPress,
  ...others
}) => {
  const [layout, setLayout] = useState();

  const { color, outlined, rounded, squared } = getVariant(variant, others);

  const colorContent = disabled ? COLOR.LIGHTEN : color === COLOR.CONTENT ? COLOR.BASE : COLOR.CONTENT;

  const size = small ? 'Small' : large ? 'Large' : '';

  const handleLayout = ({ nativeEvent: { layout } = {} } = {}) => {
    setLayout(layout);
  };

  const { spaceS } = Theme.get();

  return (
    <Touchable
      {...others}
      backgroundColor={color && disabled ? COLOR.DISABLED : color}
      borderColor={outlined ? (disabled ? COLOR.LIGHTEN : COLOR.CONTENT) : undefined}
      paddingHorizontal={!squared ? (small ? SPACE.M : SPACE.L) : undefined}
      style={[
        style.base,
        large && style.large,
        small && style.small,
        outlined && style.outlined,
        rounded && style[`rounded${size}`],
        squared && style[`squared${size}`],
        wide ? style.wide : style.fit,
        others.style,
      ]}
      wide={wide}
      onLayout={handleLayout}
      onPress={!busy && !disabled ? onPress : undefined}
    >
      <Motion delay={!busy} style={style.content} value={{ translateY: busy ? -spaceS : 0, opacity: busy ? 0 : 1 }}>
        {icon && (
          <Icon color={colorContent} name={icon} marginRight={!squared ? (small ? SPACE.XS : SPACE.S) : undefined} />
        )}

        {!squared ? (
          typeof children === 'string' ? (
            <Text action align={ALIGN.CENTER} color={colorContent} selectable={false} upperCase={upperCase}>
              {children}
            </Text>
          ) : (
            children
          )
        ) : undefined}
      </Motion>

      <Motion
        backgroundColor={outlined ? COLOR.CONTENT : COLOR.BASE}
        disabled={!busy}
        layout={layout}
        pointerEvents={POINTER.NONE}
        style={styles(style.busy, small && style.small)}
        type={MOTION_TYPES.SLOW}
        value={{ width: busy ? '100%' : '0%' }}
      />

      <Motion
        delay={busy}
        pointerEvents={POINTER.NONE}
        style={style.busyIcon}
        value={{ translateY: busy ? 0 : spaceS, opacity: busy ? 1 : 0 }}
      >
        <Icon color={colorContent} name="hourglass" />
      </Motion>
    </Touchable>
  );
};

Button.displayName = 'Button';

Button.propTypes = {
  busy: PropTypes.bool,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  disabled: PropTypes.bool,
  icon: PropTypes.string,
  large: PropTypes.bool,
  small: PropTypes.bool,
  upperCase: PropTypes.bool,
  variant: PropTypes.oneOf(Object.values(BUTTON_VARIANT)),
  wide: PropTypes.bool,
  onPress: PropTypes.func,
  // ! Will be deprecated in v4 ------------------------------------------------
  color: PropTypes.oneOf(Object.values(COLOR)),
  outlined: PropTypes.bool,
  rounded: PropTypes.bool,
  squared: PropTypes.bool,
  // !--------------------------------------------------------------------------
};

export { Button };
