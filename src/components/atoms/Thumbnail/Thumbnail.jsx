/* eslint-disable react/prop-types */
import PropTypes from 'prop-types';
import React, { useState, useLayoutEffect } from 'react';

import { ALIGN, COLOR, POINTER, POSITION, SPACE, styles } from '@hooks';
import { Image, Motion, View } from '@primitives';

// ! An atom can't import another atom
import { Skeleton } from '../Skeleton';

const Thumbnail = ({ alt, children, height, src, srcSet, width, ...others }) => {
  const [loaded, setLoaded] = useState(false);

  useLayoutEffect(() => {
    setLoaded(false);
  }, [src]);

  const handleLoad = () => {
    setLoaded(true);
  };

  const common = {
    style: styles({ height, width }, others.style),
  };

  return (
    <View position={POSITION.RELATIVE} {...others} {...common}>
      {src && <Image {...common} alt={alt} onLoad={handleLoad} resizeMode="cover" src={src} srcSet={srcSet} />}
      <Motion
        {...common}
        alignItems={ALIGN.CENTER}
        backgroundColor={COLOR.OVERLAY}
        delay={loaded}
        justifyContent={ALIGN.CENTER}
        layer={SPACE.XS}
        pointer={POINTER.NONE}
        position={POSITION.ABSOLUTE}
        value={{ opacity: loaded ? 0 : 1 }}
      >
        {!loaded && <Skeleton position={POSITION.ABSOLUTE} style={{ height: '100%', width: '100%' }} />}
        {children}
      </Motion>
    </View>
  );
};

Thumbnail.displayName = 'Thumbnail';

Thumbnail.propTypes = {
  alt: PropTypes.string,
  children: PropTypes.node,
  height: PropTypes.number,
  src: PropTypes.string.isRequired,
  srcSet: PropTypes.string,
  width: PropTypes.number,
};

export { Thumbnail };
