import PropTypes from 'prop-types';
import React from 'react';

import { Input, Select } from '@primitives';

import { Date, FieldSet, Password, Phone } from './variants';

export const HOC = ({ multiLine, options, fieldset, ...others }) => {
  const { defaultValue, type, value } = others;

  if (type === 'password') return <Password {...others} />;
  else if (type === 'tel') return <Phone {...others} />;
  else if (type === 'date') return <Date {...others} color={undefined} />;
  else if (options) return <Select {...others} color={undefined} options={options} value={value || defaultValue} />;
  else if (fieldset) return <FieldSet {...others} fields={fieldset} />;

  return <Input {...others} color={undefined} multiline={multiLine} />;
};

HOC.propTypes = {
  multiLine: PropTypes.bool,
  options: PropTypes.array,
  fieldset: PropTypes.shape({}),
};
