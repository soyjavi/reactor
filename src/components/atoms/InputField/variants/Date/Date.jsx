import PropTypes from 'prop-types';
import React, { createRef, useEffect, useState } from 'react';

import { getInputErrors } from '@helpers';
import { ALIGN, COLOR, FLEX_DIRECTION, SPACE, styles } from '@hooks';
import { Input, Text, View } from '@primitives';

import { style } from './Date.style';

const DELIMITER = '/';
const createMap = (value = '') =>
  value.split(DELIMITER).map((part) => ({ label: part, type: part[0], length: part.length || 0 }));

const Date = ({
  defaultValue,
  format = 'DD/MM/YYYY',
  formatValue = 'YYYY/MM/DD',
  max,
  min,
  value,
  onBlur,
  onChange,
  onError,
  onFocus,
  ...others
}) => {
  const [focus, setFocus] = useState(false);
  const [values, setValues] = useState(['', '', '']);

  const formatMap = createMap(format);
  const formatValueMap = createMap(formatValue);
  const dayRef = createRef();
  const monthRef = createRef();
  const yearRef = createRef();

  useEffect(() => {
    (value || defaultValue || '').split(DELIMITER).forEach((part, index) => (values[index] = part));
    setValues(values);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value, defaultValue]);

  const getRef = (type) => {
    return type === 'D' ? dayRef : type === 'M' ? monthRef : yearRef;
  };

  const handleBlur = (event) => {
    const empty = values.filter((part = '') => part.length !== 0).length === 0;
    empty && onBlur && onBlur(event);
  };

  const handleChange = (index, value = '') => {
    const { length, type } = formatValueMap[index];

    // State
    const nextValue = value.toString().substr(0, length).trim();
    const nextValues = JSON.parse(JSON.stringify(values));
    nextValues[index] = nextValue;
    setValues(nextValues);

    // Events
    const errors = getInputErrors({ ...others, value: nextValues.join('/'), min, max });
    onError && onError(errors);

    if (!errors) {
      const value = parseValue(nextValues);
      onChange(value);
      onBlur && onBlur({ customEvent: 'blurEvent' });
    }

    // Tab index
    if (nextValue.length === length) {
      const nextInputIndex = formatMap.findIndex((part) => part.type === type) + 1;
      if (formatMap[nextInputIndex]) getRef(formatMap[nextInputIndex].type).current.focus();
    }
  };

  const handleFocus = (index, event) => {
    if (!focus) {
      setFocus(true);
      getRef(formatMap[0].type).current.focus();
    }
    onFocus && onFocus(event);
  };

  const parseValue = (values) =>
    values
      .map((part, index) => {
        const { length } = formatValueMap[index];
        return part.padStart(length, '0');
      })
      .join(DELIMITER);

  return (
    <View alignItems={ALIGN.CENTER} flexDirection={FLEX_DIRECTION.ROW}>
      {formatMap.map(({ type, label, length }, index) => {
        const firstItem = index === 0;
        const lastItem = index === formatMap.length - 1;
        const mapIndex = formatValueMap.findIndex((item) => item.type === type);

        return (
          <View
            key={type}
            alignItems={ALIGN.CENTER}
            flex={lastItem ? SPACE.XS : undefined}
            flexDirection={FLEX_DIRECTION.ROW}
          >
            <Input
              {...others}
              id={`${others.id}-${type}`}
              placeholder={label}
              ref={getRef(type)}
              style={styles(
                style.input,
                !lastItem && style[`length${length}`],
                firstItem && style.firstInput,
                lastItem && style.lastInput,
                others.style,
              )}
              textAlign={lastItem ? ALIGN.LEFT : ALIGN.CENTER}
              testID={others.testID ? `${others.testID}-${type}` : undefined}
              type="number"
              value={values[mapIndex] || ''}
              onBlur={onBlur ? handleBlur : undefined}
              onChange={(value) => handleChange(mapIndex, value)}
              onFocus={(event) => handleFocus(index, event)}
            />
            {!lastItem && <Text color={others.disabled ? COLOR.LIGHTEN : undefined}>/</Text>}
          </View>
        );
      })}
    </View>
  );
};

Date.displayName = 'InputFieldDate';

Date.propTypes = {
  defaultValue: PropTypes.string,
  format: PropTypes.string,
  formatValue: PropTypes.string,
  max: PropTypes.string,
  min: PropTypes.string,
  value: PropTypes.string,
  onBlur: PropTypes.func,
  onChange: PropTypes.func.isRequired,
  onError: PropTypes.func,
  onFocus: PropTypes.func,
};

export { Date };
