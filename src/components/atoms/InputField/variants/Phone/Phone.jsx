import PropTypes from 'prop-types';
import React from 'react';

import { ALIGN, FLEX_DIRECTION, SPACE } from '@hooks';
import { Input, Text, View } from '@primitives';

const Phone = ({ color, prefix, ...others }) => (
  <View alignItems={ALIGN.CENTER} flexDirection={FLEX_DIRECTION.ROW}>
    {prefix && (
      <Text color={color} marginLeft={SPACE.M}>
        {prefix}
      </Text>
    )}
    <Input {...others} />
  </View>
);

Phone.displayName = 'InputFieldPhone';

Phone.propTypes = {
  color: PropTypes.string,
  prefix: PropTypes.string,
  onBlur: PropTypes.func,
  onChange: PropTypes.func.isRequired,
  onError: PropTypes.func,
  onFocus: PropTypes.func,
};

export { Phone };
