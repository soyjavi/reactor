import PropTypes from 'prop-types';
import React, { useState } from 'react';

import { Input } from '@primitives';

import { Button } from '../../../Button/Button';
import { style } from './Password.style';

const Password = ({ ...others }) => {
  const [visible, setVisible] = useState(false);

  return (
    <>
      <Input {...others} autoComplete="off" type={!visible ? 'password' : 'text'} />
      <Button
        aria-hidden="true"
        tabIndex={-1}
        disabled={others.disabled}
        icon={visible ? 'eye' : 'eye-off'}
        squared
        small
        style={style.button}
        onPress={() => setVisible(!visible)}
      />
    </>
  );
};

Password.displayName = 'InputFieldPassword';

Password.propTypes = {
  color: PropTypes.string,
  disabled: PropTypes.bool,
  prefix: PropTypes.string,
  onBlur: PropTypes.func,
  onChange: PropTypes.func.isRequired,
  onError: PropTypes.func,
  onFocus: PropTypes.func,
};

export { Password };
