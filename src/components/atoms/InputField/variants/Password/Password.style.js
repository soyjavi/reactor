import StyleSheet from 'react-native-extended-stylesheet';

export const style = StyleSheet.create({
  button: {
    position: 'absolute',
    top: '$spaceXS',
    right: '$spaceXS',
  },
});
