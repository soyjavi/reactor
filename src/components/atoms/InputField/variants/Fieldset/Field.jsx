import PropTypes from 'prop-types';
import React from 'react';

import { ALIGN, FLEX_DIRECTION, SPACE } from '@hooks';
import { Input, Text, View } from '@primitives';

const Field = ({ color, label, last, ...others }) => (
  <View
    alignItems={ALIGN.CENTER}
    flex={SPACE.XS}
    flexDirection={FLEX_DIRECTION.ROW}
    marginRight={last ? SPACE.M : SPACE.L}
  >
    <Input {...others} />
    <Text color={color} role="small">
      {label}
    </Text>
  </View>
);

Field.displayName = 'InputFieldFieldSetField';

Field.propTypes = {
  color: PropTypes.string,
  label: PropTypes.string,
  last: PropTypes.bool,
};

export { Field };
