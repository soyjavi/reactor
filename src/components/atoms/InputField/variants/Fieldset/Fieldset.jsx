import PropTypes from 'prop-types';
import React, { useState } from 'react';

import { getInputErrors } from '@helpers';
import { ALIGN, FLEX_DIRECTION, styles } from '@hooks';
import { View } from '@primitives';

import { Field } from './Field';
import { style } from './Fieldset.style';

const FieldSet = ({ defaultValue = {}, fields, onChange, onError, value = {}, ...others }) => {
  const [currentValue, setCurrentValue] = useState({ ...defaultValue, ...value });

  const handleChange = (field, value) => {
    setCurrentValue(() => {
      const nextValue = { ...currentValue, [field]: value };
      const errors = getInputErrors({ ...others, value: nextValue });

      onError(errors);
      onChange(nextValue);

      return nextValue;
    });
  };

  const fieldKeys = Object.keys(fields);

  return (
    <View alignItems={ALIGN.CENTER} flexDirection={FLEX_DIRECTION.ROW} style={styles(others.style)}>
      {fieldKeys.map((field, index) => (
        <Field
          {...others}
          key={field}
          defaultValue={defaultValue[field]}
          id={`${others.id}-${field}`}
          label={field}
          last={fieldKeys.length === 1 || index === fieldKeys.length - 1}
          role="spinbutton"
          style={style.field}
          testID={others.testID ? `${others.testID}-${field}` : undefined}
          type={fields[field]}
          value={value[field]}
          onChange={(value) => handleChange(field, value)}
        />
      ))}
    </View>
  );
};

FieldSet.displayName = 'InputFieldFieldSet';

FieldSet.propTypes = {
  defaultValue: PropTypes.shape({}),
  fields: PropTypes.shape({}).isRequired,
  test: PropTypes.func,
  value: PropTypes.shape({}),
  onBlur: PropTypes.func,
  onChange: PropTypes.func.isRequired,
  onError: PropTypes.func,
  onFocus: PropTypes.func,
};
export { FieldSet };
