import PropTypes from 'prop-types';
import React, { useState } from 'react';

import { COLOR, DISPLAY, POINTER, POSITION, SPACE, styles, useBanProps, useBanStylerProps, useId } from '@hooks';
import { Icon, Motion, Text, View, INPUT_TYPES } from '@primitives';
import { Theme } from '@theming';

import { useField } from './helpers';
import { BANNED_PROPS } from './InputField.definition';
import { HOC } from './InputField.hoc';
import { style } from './InputField.style';

const MOTION_M_DELAY = 300;

const InputField = ({
  disabled = false,
  error = false,
  fieldset,
  hint,
  icon,
  id,
  label,
  multiLine = false,
  name,
  style: inheritStyle,
  type,
  onBlur,
  onChange,
  onFocus,
  options,
  ...others
}) => {
  const idField = useId(id);
  const { handleChange, filled } = useField({ onChange, ...others });

  const [focus, setFocus] = useState(false);
  const [timeoutID, setTimeoutID] = useState(undefined);

  const handleBlur = (event) => {
    if (disabled || !focus) return;

    setFocus(false);
    setTimeoutID(onBlur ? setTimeout(() => onBlur(event), MOTION_M_DELAY) : undefined);
  };

  const handleFocus = (event) => {
    clearTimeout(timeoutID);
    if (disabled) return;

    if (!focus) {
      setFocus(true);
      if (onFocus) onFocus(event);
    }
  };

  const { spaceXS, spaceS, spaceM } = Theme.get();

  const empty = !filled && !focus;

  let color = COLOR.LIGHTEN;
  if (disabled) color = COLOR.LIGHTEN;
  else if (error) color = COLOR.ERROR;
  else if (focus) color = COLOR.CONTENT;

  return (
    <View
      {...useBanProps(others, BANNED_PROPS)}
      style={styles(
        style.inputField,
        focus && style.focus,
        disabled && style.disabled,
        empty && !error && style.empty,
        error && style.error,
        (filled || error) && style.filled,
        inheritStyle,
      )}
    >
      <View
        backgroundColor={COLOR.BASE}
        borderColor={error ? COLOR.ERROR : focus ? COLOR.CONTENT : COLOR.LIGHTEN}
        position={POSITION.RELATIVE}
        style={style.border}
        testID={others.testID}
      >
        <HOC
          {...useBanStylerProps(others)}
          color={color}
          disabled={disabled}
          fieldset={fieldset}
          id={idField}
          label={label}
          name={name}
          multiLine={multiLine}
          onBlur={handleBlur}
          onChange={handleChange}
          onFocus={handleFocus}
          options={options}
          paddingLeft={SPACE.M}
          paddingRight={icon ? SPACE.XXL : SPACE.M}
          position={POSITION.RELATIVE}
          style={styles(style.content, multiLine && style.multiline, empty && !error && style.contentEmpty)}
          type={type}
        />

        <Motion
          backgroundColor={COLOR.BASE}
          borderColor={!empty ? COLOR.SECONDARY : undefined}
          color={color}
          display={DISPLAY.BLOCK}
          layer={SPACE.XS}
          marginHorizontal={SPACE.M}
          pointer={POINTER.NONE}
          position={POSITION.ABSOLUTE}
          style={styles(style.label, empty && !error && style.labelEmpty)}
          value={{
            translateY: empty && !error ? spaceM : Number(spaceS) * -1,
            translateX: empty && !error ? 0 : -Number(spaceXS),
          }}
        >
          <Text role="label" color={color} ellipsizeMode htmlFor={idField} small={!empty || error}>
            {label}
          </Text>
        </Motion>

        {icon && (
          <Icon
            color={color}
            name={icon}
            pointerEvents={POINTER.NONE}
            position={POSITION.ABSOLUTE}
            style={style.icon}
          />
        )}
      </View>
      {!!hint && (
        <Text color={color} display={DISPLAY.INLINE_BLOCK} marginLeft={SPACE.M} marginTop={SPACE.XS} small>
          {hint}
        </Text>
      )}
    </View>
  );
};

InputField.displayName = 'InputField';

InputField.propTypes = {
  defaultValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.shape({})]),
  disabled: PropTypes.bool,
  error: PropTypes.bool,
  emptyOptionText: PropTypes.string,
  fieldset: PropTypes.shape({}),
  format: PropTypes.string,
  formatValue: PropTypes.string,
  hint: PropTypes.string,
  icon: PropTypes.string,
  id: PropTypes.string,
  label: PropTypes.string.isRequired,
  multiLine: PropTypes.bool,
  name: PropTypes.string.isRequired,
  options: PropTypes.array,
  style: PropTypes.oneOfType([PropTypes.number, PropTypes.object, PropTypes.array]),
  type: PropTypes.oneOf(INPUT_TYPES),
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  onError: PropTypes.func,
  onFocus: PropTypes.func,
};

export { InputField };
