const BANNED_PROPS = ['format', 'formatValue', 'max', 'min', 'test', 'testID'];

export { BANNED_PROPS };
