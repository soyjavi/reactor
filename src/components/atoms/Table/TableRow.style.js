import StyleSheet from 'react-native-extended-stylesheet';

export const style = StyleSheet.create({
  tableRow: {
    flexDirection: 'row',
  },

  th: {
    borderBottomColor: '$colorLighten',
    borderBottomWidth: '$borderSize',
    borderStyle: '$borderStyle',
    flex: 1,
  },

  td: {
    borderBottomColor: '$colorLighten',
    borderBottomWidth: '$borderSize',
    borderStyle: '$borderStyle',
    flex: 1,
  },
});
