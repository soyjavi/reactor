import PropTypes from 'prop-types';
import React from 'react';

import { ALIGN } from '@hooks';
import { View } from '@primitives';

import { TableRow } from './TableRow';

const Table = ({ dataSource = [], schema, ...others }) => (
  <View {...others} role="table" textAlign={ALIGN.LEFT} wide>
    <View role="thead">
      <TableRow schema={schema} />
    </View>
    <View role="tbody">
      {dataSource.map((row, index) => (
        <TableRow dataSource={row} key={index} schema={schema} />
      ))}
    </View>
  </View>
);

Table.displayName = 'Table';

Table.propTypes = {
  dataSource: PropTypes.arrayOf(PropTypes.shape({})),
  schema: PropTypes.shape({}).isRequired,
};

export { Table };
