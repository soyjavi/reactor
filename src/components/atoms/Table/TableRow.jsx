import PropTypes from 'prop-types';
import React from 'react';

import { COLOR, SPACE, styles } from '@hooks';
import { Text, View } from '@primitives';

import { style } from './TableRow.style';

const TableRow = ({ dataSource, schema, ...others }) => {
  const isHead = dataSource === undefined;
  const as = isHead ? 'th' : 'td';

  const fields = Object.keys(schema);

  return (
    <View {...others} role="tr" style={styles(style.tableRow, others.style)}>
      {fields.map((field, index) => (
        <View
          as={as}
          key={field}
          paddingRight={index < fields.length - 1 ? SPACE.M : undefined}
          paddingVertical={SPACE.M}
          style={style[as]}
        >
          <Text color={isHead ? COLOR.LIGHTEN : undefined} small upperCase>
            {isHead ? schema[field] : dataSource[field]}
          </Text>
        </View>
      ))}
    </View>
  );
};

TableRow.displayName = 'TableRow';

TableRow.propTypes = {
  dataSource: PropTypes.shape({}),
  schema: PropTypes.shape({}).isRequired,
};

export { TableRow };
