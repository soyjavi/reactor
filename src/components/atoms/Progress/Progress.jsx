import PropTypes from 'prop-types';
import React from 'react';

import { COLOR, FLEX_DIRECTION, SPACE } from '@hooks';
import { Touchable, View } from '@primitives';

import { style } from './Progress.style';

const Progress = ({
  active = 0,
  activeColor = COLOR.CONTENT,
  inactiveColor = COLOR.LIGHTEN,
  length = 1,
  onPress,
  ...others
}) => (
  <View {...others} flexDirection={FLEX_DIRECTION.ROW} wide>
    {Array.from(Array(length).keys()).map((step, index) => (
      <Touchable
        backgroundColor={index <= active ? activeColor : inactiveColor}
        flex={SPACE.XS}
        hitSlop={{ top: 16, bottom: 16 }}
        key={step}
        marginRight={index < length - 1 ? SPACE.XS : undefined}
        style={style.item}
        onPress={onPress ? () => onPress(index) : undefined}
      />
    ))}
  </View>
);

Progress.propTypes = {
  active: PropTypes.number,
  activeColor: PropTypes.oneOf(Object.values(COLOR)),
  inactiveColor: PropTypes.oneOf(Object.values(COLOR)),
  length: PropTypes.number,
  onPress: PropTypes.func,
};

export { Progress };
