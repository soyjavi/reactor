import PropTypes from 'prop-types';
import React from 'react';

import { FLEX_DIRECTION } from '@hooks';
import { ScrollView } from '@primitives';

const Slider = ({ children, selected = 0, snapInterval, onChange, ...others }) => {
  const handleScroll = ({ X } = {}) => {
    const next = X < snapInterval ? 0 : Math.round(X / snapInterval);

    if (onChange && next !== selected) onChange(next);
  };

  return (
    <ScrollView
      {...others}
      animated
      flexDirection={FLEX_DIRECTION.ROW}
      horizontal
      scrollTo={selected * snapInterval}
      snapInterval={snapInterval}
      onScroll={handleScroll}
    >
      {React.Children.map(children, (child, index) => React.cloneElement(child, { key: index }))}
    </ScrollView>
  );
};

Slider.displayName = 'Slider';

Slider.propTypes = {
  children: PropTypes.node.isRequired,
  selected: PropTypes.number,
  snapInterval: PropTypes.number,
  onChange: PropTypes.func,
};

export { Slider };
