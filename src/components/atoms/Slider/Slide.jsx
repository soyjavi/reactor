import PropTypes from 'prop-types';
import React from 'react';

import { View } from '@primitives';

// ! An atom can't import another atom
import { Thumbnail } from '../Thumbnail';

const Slide = ({ children, height = 320, src, width = 256, ...others }) => (
  <View {...others}>
    <Thumbnail src={src} height={height} width={width} />
    {children}
  </View>
);

Slide.displayName = 'Slide';

Slide.propTypes = {
  active: PropTypes.bool,
  children: PropTypes.node,
  height: PropTypes.number,
  src: PropTypes.string,
  width: PropTypes.number,
};

export { Slide };
