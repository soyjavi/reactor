import { LinearGradient } from 'expo-linear-gradient';
import PropTypes from 'prop-types';
import React, { useLayoutEffect, useState } from 'react';

import { capitalize, IS_JEST } from '@helpers';
import { COLOR, styles, useDevice } from '@hooks';
import { Motion, Text, View } from '@primitives';
import { Theme } from '@theming';

import { BASE_TRANSPARENT, TEXT_GAP } from './Skeleton.definition';
import { style } from './Skeleton.style';

const Skeleton = ({ backgroundColor = COLOR.LIGHTEN, children, color = COLOR.LIGHTEN, height, width, ...others }) => {
  const { screen } = useDevice();

  const [layout, setLayout] = useState();

  useLayoutEffect(() => {
    if (height && width) setLayout({ height, width });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleLayout = ({ nativeEvent: { layout } }) => {
    setLayout(height && width ? { height, width } : layout);
  };

  const { motionSkeleton = 1200, [`color${capitalize(color)}`]: hexColor } = Theme.get();

  return (
    <View {...others}>
      <View
        backgroundColor={backgroundColor}
        style={styles(
          style.container,
          layout ? { height: layout.height, width: layout.width + (children ? TEXT_GAP : 0) } : { height, width },
          others.style,
        )}
        onLayout={!children && !layout ? handleLayout : undefined}
      >
        <Motion
          disabled={!layout || IS_JEST}
          duration={motionSkeleton}
          loops={layout ? -1 : undefined}
          style={style.content}
          value={{
            left: 0,
            translateX: screen.width * (layout ? 1 : -1),
          }}
        >
          <LinearGradient
            colors={[BASE_TRANSPARENT, hexColor, hexColor, BASE_TRANSPARENT]}
            end={{ x: 1, y: 0 }}
            locations={[0, 0.25, 0.75, 1]}
            start={{ x: 0, y: 0 }}
            style={style.content}
          />
        </Motion>

        {children && (
          <Text {...others} aria-hidden="true" style={style.text} onLayout={handleLayout}>
            {children}
          </Text>
        )}
      </View>
    </View>
  );
};

Skeleton.displayName = 'Skeleton';

Skeleton.propTypes = {
  backgroundColor: PropTypes.oneOf(Object.values(COLOR)),
  children: PropTypes.string,
  color: PropTypes.oneOf(Object.values(COLOR)),
  text: PropTypes.bool,
  height: PropTypes.number,
  width: PropTypes.number,
};

export { Skeleton };
