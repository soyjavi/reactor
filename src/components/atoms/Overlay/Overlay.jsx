import PropTypes from 'prop-types';
import React from 'react';

import { ALIGN, COLOR, FLEX_DIRECTION, POSITION, SPACE, styles } from '@hooks';
import { Motion, Touchable } from '@primitives';

import { style } from './Overlay.style';

const Overlay = ({ children, isVisible, onPress, ...others }) => (
  <Motion
    {...others}
    backgroundColor={COLOR.OVERLAY}
    delay={!isVisible}
    layer={SPACE.M}
    position={POSITION.FIXED}
    style={styles(style.overlay, others.style)}
    type={isVisible ? 'expand' : 'collapse'}
    value={{ opacity: isVisible ? 1 : 0 }}
  >
    <Touchable
      alignItems={ALIGN.CENTER}
      flexDirection={FLEX_DIRECTION.COLUMN}
      justifyContent={ALIGN.CENTER}
      style={style.touchable}
      tabIndex={0}
      onPress={onPress}
    >
      {children}
    </Touchable>
  </Motion>
);

Overlay.displayName = 'Overlay';

Overlay.propTypes = {
  children: PropTypes.node,
  isVisible: PropTypes.bool,
  onPress: PropTypes.func,
};

export { Overlay };
