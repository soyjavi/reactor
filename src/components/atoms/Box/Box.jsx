import PropTypes from 'prop-types';
import React from 'react';

import { ALIGN, styles, useDevice } from '@hooks';
import { View } from '@primitives';

import { style } from './Box.style';
import { determineSize } from './helpers';

const Box = React.forwardRef(({ align, children, size, ...others }, ref) => {
  const { screen } = useDevice();

  return (
    <View
      {...others}
      ref={ref}
      style={styles(
        !screen.S || (size && Object.keys(size).includes('S'))
          ? styles(style[determineSize(size, screen)], style[align])
          : undefined,
        others.style,
      )}
      wide
    >
      {children}
    </View>
  );
});

Box.displayName = 'Box';

Box.propTypes = {
  align: PropTypes.oneOf([ALIGN.LEFT, ALIGN.CENTER, ALIGN.RIGHT]),
  children: PropTypes.node,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
};

export { Box };
