import PropTypes from 'prop-types';
import React from 'react';

import { COLOR, DISPLAY, FLEX_DIRECTION, POINTER, POSITION, SPACE, useBanStylerProps } from '@hooks';
import { Icon, Motion, Switch, SWITCH_VARIANT, Text, View } from '@primitives';

import { style } from './InputOption.style';

export const InputOption = ({
  checked = false,
  children,
  hint,
  id,
  name,
  onChange,
  type = SWITCH_VARIANT.RADIO,
  value,
  ...others
}) => (
  <View {...others} accessibilityLabel={hint} flexDirection={FLEX_DIRECTION.ROW}>
    <View position={POSITION.RELATIVE} style={style.input}>
      {React.createElement(Switch, {
        ...useBanStylerProps({ ...others, testID: undefined }),
        checked,
        id,
        name,
        value,
        variant: type,
        onChange,
      })}
      <Motion
        pointer={POINTER.NONE}
        position={POSITION.ABSOLUTE}
        style={style.icon}
        type={checked ? 'expand' : 'collapse'}
        value={{ opacity: checked ? 1 : 0, scale: checked ? 1 : 0 }}
      >
        <Icon color={others.disabled ? COLOR.LIGHTEN : undefined} name="tick" />
      </Motion>
    </View>
    {(children || hint) && (
      <View wide>
        {children}
        {!!hint && (
          <Text
            color={COLOR.LIGHTEN}
            display={DISPLAY.INLINE_BLOCK}
            level={2}
            marginLeft={SPACE.M}
            marginTop={SPACE.XS}
            small
          >
            {hint}
          </Text>
        )}
      </View>
    )}
  </View>
);

InputOption.displayName = 'InputOption';

InputOption.propTypes = {
  checked: PropTypes.bool,
  hint: PropTypes.string,
  id: PropTypes.string,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  type: PropTypes.oneOf(Object.values(SWITCH_VARIANT)),
  value: PropTypes.string.isRequired,
  children: PropTypes.node,
};
