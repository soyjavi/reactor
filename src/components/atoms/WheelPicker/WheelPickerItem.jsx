import PropTypes from 'prop-types';
import React from 'react';

import { ALIGN, COLOR, POINTER, SPACE, styles } from '@hooks';
import { Text, Touchable } from '@primitives';

import { style } from './WheelPickerItem.style';

export const WheelPickerItem = ({ checked, disabled, onPress, scrollable, title, testID, ...others }) => (
  <Touchable
    alignItems={ALIGN.CENTER}
    role="spinbutton"
    backgroundColor={checked && !scrollable ? COLOR.SECONDARY : undefined}
    borderColor={checked && !scrollable ? (disabled ? COLOR.LIGHTEN : COLOR.SECONDARY) : COLOR.BASE}
    justifyContent={ALIGN.CENTER}
    layer={checked ? SPACE.XS : undefined}
    paddingHorizontal={SPACE.M}
    style={styles(style.item, !scrollable && style.itemHighlight, others.style)}
    testID={testID}
    wide
    onPress={!disabled ? onPress : undefined}
  >
    {title && (
      <Text
        action={!scrollable}
        align={ALIGN.CENTER}
        role="label"
        color={disabled ? COLOR.LIGHTEN : !checked ? COLOR.LIGHTEN : undefined}
        htmlFor={title}
        pointer={POINTER.NONE}
        selectable={false}
        small={scrollable}
      >
        {title}
      </Text>
    )}
  </Touchable>
);

WheelPickerItem.displayName = 'WheelPickerItem';

WheelPickerItem.propTypes = {
  behavior: PropTypes.oneOf(['smooth', 'auto']),
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
  onPress: PropTypes.func,
  scrollable: PropTypes.bool,
  testID: PropTypes.string,
  title: PropTypes.string,
};
