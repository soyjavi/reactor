import { LinearGradient } from 'expo-linear-gradient';
import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { Platform } from 'react-native';

import { COLOR, FLEX_DIRECTION, POINTER, POSITION, SPACE, styles, useStyler } from '@hooks';
import { ScrollView, View } from '@primitives';
import { Theme } from '@theming';

import { BASE_TRANSPARENT, VISIBLE_ITEMS } from './WheelPicker.definition';
import { style } from './WheelPicker.style';
import { WheelPickerItem } from './WheelPickerItem';

const WheelPicker = ({
  disabled = false,
  onChange,
  options = [],
  selected,
  selectOnScroll = true,
  testID,
  ...others
}) => {
  const [current, setCurrent] = useState();

  const { colorBase, inputSize } = Theme.get();

  useEffect(() => {
    if (selected !== current) setCurrent(selected);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selected]);

  const handlePressItem = (nextOption) => {
    handleChange(nextOption);
  };

  const handleScroll = ({ Y }) => {
    handleChange(Y < inputSize ? 0 : Math.round(Y / inputSize));
  };

  const handleChange = (nextOption) => {
    if (nextOption !== current) {
      setCurrent(nextOption);
      if (onChange) onChange(nextOption);
    }
  };

  const scrollable = options.length > VISIBLE_ITEMS;
  const gradientProps = useStyler({ wide: true, style: style.gradient });

  return (
    <View {...others} position={POSITION.RELATIVE} wide>
      <ScrollView
        animated={!disabled && Platform.OS === 'web'}
        height={(scrollable ? VISIBLE_ITEMS : options.length) * inputSize}
        nestedScrollEnabled
        role="ul"
        scrollTo={current * inputSize}
        snapInterval={inputSize}
        style={styles(others.style)}
        onScroll={!disabled && selectOnScroll && scrollable ? handleScroll : undefined}
        testID={testID}
      >
        {scrollable && (
          <>
            <WheelPickerItem disabled scrollable />
            <WheelPickerItem disabled scrollable />
          </>
        )}

        {options.map((option, index) => (
          <WheelPickerItem
            checked={index === current}
            disabled={disabled}
            key={option}
            scrollable={scrollable}
            testID={`${testID}-${index}`}
            title={option}
            onPress={() => handlePressItem(index)}
          />
        ))}

        {scrollable && (
          <>
            <WheelPickerItem disabled scrollable />
            <WheelPickerItem disabled scrollable />
          </>
        )}
      </ScrollView>

      {scrollable && (
        <View
          flexDirection={FLEX_DIRECTION.COLUMN}
          layer={SPACE.XS}
          pointerEvents={POINTER.NONE}
          position={POSITION.ABSOLUTE}
          style={style.dartBoard}
          wide
        >
          <LinearGradient {...gradientProps} colors={[colorBase, BASE_TRANSPARENT]} />
          <View borderColor={disabled ? COLOR.LIGHTEN : COLOR.CONTENT} style={style.selector} wide />
          <LinearGradient {...gradientProps} colors={[BASE_TRANSPARENT, colorBase]} />
        </View>
      )}
    </View>
  );
};

WheelPicker.displayName = 'WheelPicker';

WheelPicker.propTypes = {
  disabled: PropTypes.bool,
  options: PropTypes.array.isRequired,
  selected: PropTypes.number,
  selectOnScroll: PropTypes.bool,
  testID: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

export { WheelPicker };
