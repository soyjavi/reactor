/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable react/prop-types */
import PropTypes from 'prop-types';
import React, { useEffect, useMemo, useState } from 'react';

import { INPUT_TYPES, View } from '@primitives';

import { groupState, getField, getChildrenErrors, getChildrenValues } from './helpers';

const Form = ({
  children,
  debounce = 0,
  schema = {},
  validateOnMount = false,
  onBlur,
  onChange,
  onError,
  onFocus,
  onSubmit,
  ...others
}) => {
  const [error, setError] = useState({});
  const [initialValue, setInitialValue] = useState({});
  const [touched, setTouched] = useState({});
  const [values, setValues] = useState({});

  useEffect(() => {
    const nextValues = getChildrenValues(children);
    const nextChildrenKeys = Object.keys(nextValues).sort();

    if (JSON.stringify(nextChildrenKeys) !== JSON.stringify(Object.keys(initialValue).sort())) {
      setInitialValue(nextValues);
      setValues(nextValues);

      if (validateOnMount) {
        const nextError = getChildrenErrors({ children, schema, values: nextValues });
        setError(nextError);
        onError && onError(nextError);
      } else {
        setError({});
      }
      setTouched({});
    }
    // ! TODO: Probably handleChange() sets the value correctly, this condition is not necessary.
    // else {
    //   const collision = nextChildrenKeys.some((key) => JSON.stringify(values[key]) !== JSON.stringify(nextValues[key]));
    //   if (collision) setValues(nextValues);
    // }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [children]);

  useEffect(() => {
    if (!onChange || values === initialValue) return;

    const timer = setTimeout(() => onChange(values, groupState({ initialValue, value: values, touched })), debounce);
    return () => clearTimeout(timer);
  }, [values]);

  const handleChange = (field, fieldValue) => {
    setValues(() => {
      const nextValues = { ...values, [field]: fieldValue };

      handleError(nextValues);

      return nextValues;
    });
  };

  const handleError = (values) => {
    const nextError = getChildrenErrors({ children, schema, values });
    const hasError = Object.keys(nextError).length > 0;
    const changed = JSON.stringify(error) !== JSON.stringify(nextError);

    if (changed) {
      setError(nextError);
      onError && onError(nextError, hasError);
    }

    return { changed, errors: nextError, hasError };
  };

  const handleFocus = (field, event) => {
    setTouched({ ...touched, [field]: true });
    if (onFocus) onFocus(field, event);
  };

  const handleSubmit = (event) => {
    const { errors, hasError } = handleError(values);

    if (hasError && onError) onError(errors, hasError);
    else if (onSubmit) onSubmit(values, groupState({ initialValue, value: values, touched }), event);
  };

  return useMemo(
    () => (
      <View {...others} role="form">
        {React.Children.map(children, (child, index) => {
          if (!child || child === null) return;

          const { props = {} } = child || {};
          const { type } = props;
          const field = getField(props);

          return React.cloneElement(child, {
            key: index,
            ...(field
              ? {
                  ...props,
                  ...schema[field],
                  error: error[field] !== undefined,
                  onBlur: onBlur ? (event) => onBlur(field, event) : undefined,
                  onChange: (value) => handleChange(field, value),
                  onFocus: (event) => handleFocus(field, event),
                  ...(INPUT_TYPES.includes(type) || !props.options ? { onSubmitEditing: handleSubmit } : undefined),
                }
              : type === 'submit'
              ? { ...props, onPress: handleSubmit }
              : undefined),
          });
        })}
      </View>
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [children, error, others, schema],
  );
};

Form.propTypes = {
  children: PropTypes.node,
  debounce: PropTypes.number,
  schema: PropTypes.shape({}),
  validateOnMount: PropTypes.bool,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  onError: PropTypes.func,
  onFocus: PropTypes.func,
  onSubmit: PropTypes.func,
};

export { Form };
