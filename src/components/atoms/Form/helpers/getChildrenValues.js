import React from 'react';

import { BOOLEAN_TYPES } from '../Form.definition';
import { getField } from './getField';

export const getChildrenValues = (children) => {
  const values = {};

  React.Children.forEach(
    children,
    ({
      props: {
        checked = false,
        defaultChecked = false,
        defaultValue = undefined,
        type = undefined,
        value = undefined,
        ...props
      } = {},
    } = {}) => {
      const field = getField(props);
      if (field) values[field] = BOOLEAN_TYPES.includes(type) ? checked || defaultChecked : value || defaultValue;
    },
  );

  return values;
};
