export const getCaption = ({ num, text }) => {
  if (isNaN(num) || !text) throw new Error('getCaption: incorrect values');
  return `${num}-${text}`;
};
