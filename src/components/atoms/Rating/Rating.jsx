import PropTypes from 'prop-types';
import React from 'react';

import { ALIGN, COLOR, DISPLAY, FLEX_DIRECTION, SPACE } from '@hooks';
import { Text, Touchable, View } from '@primitives';

import { getCaption } from './helpers';
import { style } from './Rating.style';

const Rating = ({ max = 5, maxCaption, min = 1, minCaption, title, value, onChange, ...others }) => {
  const ratingValues = Array.from(Array(max), (item, index) => index + min);

  const color = value ? COLOR.LIGHTEN : COLOR.CONTENT;

  const small = max > 5;

  return (
    <View {...others}>
      {title && (
        <Text align={ALIGN.CENTER} display={DISPLAY.BLOCK} headline level={4} marginBottom={SPACE.M} upperCase>
          {title}
        </Text>
      )}

      <View flexDirection={FLEX_DIRECTION.ROW} justifyContent={ALIGN.BETWEEN}>
        {ratingValues.map((ratingValue) => {
          const active = value === ratingValue;

          return (
            <View
              flexDirection={FLEX_DIRECTION.COLUMN}
              justifyContent={ALIGN.CENTER}
              alignItems={ALIGN.CENTER}
              key={ratingValue}
            >
              {small && (
                <Text
                  align={ALIGN.CENTER}
                  color={COLOR.CONTENT}
                  marginBottom={SPACE.XS}
                  action
                  style={style.value}
                  wide
                >
                  {ratingValue}
                </Text>
              )}

              <Touchable
                alignItems={ALIGN.CENTER}
                borderColor={active ? COLOR.SECONDARY : color}
                backgroundColor={active ? COLOR.SECONDARY : COLOR.BASE}
                justifyContent={ALIGN.CENTER}
                key={ratingValue}
                padding={SPACE.RESET}
                style={small ? style.itemSmall : style.item}
                onPress={onChange ? () => onChange(ratingValue) : undefined}
              >
                {!small && (
                  <Text action align={ALIGN.CENTER} color={active ? COLOR.CONTENT : color} style={style.value} wide>
                    {ratingValue}
                  </Text>
                )}
              </Touchable>
            </View>
          );
        })}
      </View>

      {minCaption && maxCaption && (
        <View flexDirection={FLEX_DIRECTION.ROW} justifyContent={ALIGN.BETWEEN} marginTop={SPACE.M}>
          <Text color={COLOR.CONTENT} small upperCase>
            {getCaption({ num: min, text: minCaption })}
          </Text>
          <Text color={COLOR.CONTENT} small upperCase>
            {getCaption({ num: max, text: maxCaption })}
          </Text>
        </View>
      )}
    </View>
  );
};

Rating.displayName = 'Rating';

Rating.propTypes = {
  max: PropTypes.number,
  maxCaption: PropTypes.string,
  min: PropTypes.number,
  minCaption: PropTypes.string,
  title: PropTypes.string,
  value: PropTypes.number,
  onChange: PropTypes.func,
};

export { Rating };
