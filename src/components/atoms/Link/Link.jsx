import PropTypes from 'prop-types';
import React from 'react';
import { Linking } from 'react-native';

import { ALIGN, COLOR, POINTER, useKeyboard } from '@hooks';
import { Text, Touchable } from '@primitives';

const Link = ({ align, children, color = COLOR.CONTENT, href, testID, underlined, onPress, ...others }) => {
  const { isKeyboardOpen, closeKeyboard } = useKeyboard();

  const hasHref = href && href.length > 0;

  const handlePress = () => {
    isKeyboardOpen && closeKeyboard();
    if (onPress) onPress({ href });
    else if (hasHref) Linking.openURL(href);
  };

  return (
    <Touchable
      alignSelf={align}
      feedback={false}
      href={hasHref ? href : undefined}
      role="link"
      testID={testID}
      onPress={handlePress}
    >
      <Text
        {...others}
        align={align}
        borderColor={underlined ? color : undefined}
        color={color}
        pointerEvents={POINTER.AUTO}
        selectable={false}
        underlined={underlined}
      >
        {children}
      </Text>
    </Touchable>
  );
};

Link.displayName = 'Link';

Link.propTypes = {
  align: PropTypes.oneOf(Object.values(ALIGN)),
  children: PropTypes.node.isRequired,
  color: PropTypes.string,
  href: PropTypes.string,
  testID: PropTypes.string,
  underlined: PropTypes.bool,
  onPress: PropTypes.func,
};

export { Link };
