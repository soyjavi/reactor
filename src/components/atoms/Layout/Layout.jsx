import PropTypes from 'prop-types';
import React from 'react';

import { ALIGN, SPACE, styles, useDevice } from '@hooks';
import { View } from '@primitives';

import { style } from './Layout.style';

const Layout = ({
  children,
  alignItems = ALIGN.START,
  fullWidth,
  inline = true,
  justifyContent = ALIGN.BETWEEN,
  reverse,
  ...others
}) => {
  const { screen } = useDevice();

  return (
    <View
      {...others}
      alignItems={alignItems}
      justifyContent={justifyContent}
      paddingHorizontal={!fullWidth ? (screen.S ? SPACE.XL : SPACE.XXL) : undefined}
      style={styles(
        style.layout,
        inline ? style.row : style.column,
        reverse && (inline && !screen.S ? style.reverse : style.reverseColumn),
        fullWidth && style.fullWidth,
        others.style,
      )}
      wide
    >
      {children}
    </View>
  );
};

Layout.displayName = 'Layout';

Layout.propTypes = {
  alignItems: PropTypes.oneOf([ALIGN.START, ALIGN.CENTER, ALIGN.END]),
  children: PropTypes.node.isRequired,
  fullWidth: PropTypes.bool,
  inline: PropTypes.bool,
  justifyContent: PropTypes.oneOf([ALIGN.BETWEEN, ALIGN.AROUND, ALIGN.END, ALIGN.CENTER, ALIGN.START]),
  reverse: PropTypes.bool,
};

export { Layout };
