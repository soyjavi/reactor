import PropTypes from 'prop-types';

import { StylerPropTypes } from '@hooks';

export const PrimitivePropTypes = {
  style: PropTypes.oneOfType([PropTypes.number, PropTypes.object, PropTypes.array]),
  // TODO should remove --------------------------------------------------------
  'aria-describedby': PropTypes.string,
  // TODO ----------------------------------------------------------------------
  accesibilityLabel: PropTypes.string,
  id: PropTypes.string,
  testID: PropTypes.string,
  ...StylerPropTypes,
};
