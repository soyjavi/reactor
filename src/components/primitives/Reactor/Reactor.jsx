import PropTypes from 'prop-types';
import React, { useMemo } from 'react';
import { View } from 'react-native';

import { DeviceProvider, PortalProvider, StackProvider } from '@contexts';
import { styles, useBanProps, useBanStylerProps, useStyler } from '@hooks';
import { DefaultTheme, Theme } from '@theming';

import { BANNED_PROPS } from './Reactor.definition';
import { style } from './Reactor.style';

const Reactor = ({ children, forceViewport, theme = DefaultTheme, useStack = true, usePortal = true, ...others }) => {
  useMemo(() => Theme.set(theme), [theme]);

  const Stack = useStack ? StackProvider : React.Fragment;
  const Portal = usePortal ? PortalProvider : React.Fragment;

  return (
    <View
      {...useBanProps(useBanStylerProps(others), BANNED_PROPS)}
      {...useStyler({ ...others, style: styles(style.container, others.style) })}
    >
      <DeviceProvider forceViewport={forceViewport}>
        <Stack>
          <Portal>{children}</Portal>
        </Stack>
      </DeviceProvider>
    </View>
  );
};

Reactor.displayName = 'Reactor';

Reactor.propTypes = {
  children: PropTypes.node.isRequired,
  forceViewport: PropTypes.shape({
    height: PropTypes.number,
    width: PropTypes.number,
  }),
  theme: PropTypes.shape({}),
  usePortal: PropTypes.bool,
  useStack: PropTypes.bool,
};

export { Reactor };
