import StyleSheet from 'react-native-extended-stylesheet';

import { DefaultTheme } from '@theming';

StyleSheet.build(DefaultTheme);

export const style = StyleSheet.create({
  container: {
    backgroundColor: '$colorBase',
    flex: 1,
  },
});
