/* eslint-disable react-hooks/rules-of-hooks */
import PropTypes from 'prop-types';
import React from 'react';
import { Platform, Text as BaseText } from 'react-native';

import { capitalize, stylesheetColor, stylesheetFont, testID } from '@helpers';
import {
  // Cont
  ALIGN,
  POINTER,
  // Methods
  styles,
  // Hooks
  useBanProps,
  useBanStylerProps,
  useDevice,
  useStyler,
} from '@hooks';

import { PrimitivePropTypes } from '../Primitive.definition';
import { BANNED_PROPS, TEXT_ROLES } from './Text.definition';
import { style } from './Text.style';

const Text = (props) => {
  if (React.isValidElement(props.children)) {
    const { children, ...clonedProps } = props;

    return React.cloneElement(children, clonedProps);
  }

  const {
    action,
    align,
    color,
    children = '',
    displayName = null,
    headline,
    hidden,
    level = 1,
    ellipsizeMode,
    lineThrough,
    role,
    selectable = false,
    small,
    underlined,
    ...others
  } = props;

  return (
    <BaseText
      {...useBanStylerProps(useBanProps(others, BANNED_PROPS))}
      {...useStyler(
        {
          ...others,
          textAlign: align,
          style: styles(
            stylesheetFont({
              nameSpace: `$font${capitalize(
                headline ? 'headline' : action ? 'action' : small ? 'small' : 'paragraph',
              )}`,
              level: headline ? level.toString() : undefined,
            }).font,
            stylesheetColor(color).color,
            hidden && style.hidden,
            lineThrough && style.lineThrough,
            underlined && style.underlined,
            align && style[align],
            others.style,
          ),
        },
        displayName,
        useDevice(),
      )}
      {...testID(others.testID)}
      {...(ellipsizeMode ? { numberOfLines: 1 } : undefined)}
      accessibilityRole={Platform.OS === 'web' ? role : undefined}
      allowFontScaling={false}
      pointerEvents={others.pointerEvents || (!selectable || Platform.OS !== 'web' ? POINTER.NONE : undefined)}
      selectable={selectable}
    >
      {Array.isArray(children)
        ? React.Children.map(children, (child) => {
            // eslint-disable-next-line no-unused-vars
            const { children, ...clonedProps } = props;

            return React.isValidElement(child) ? React.cloneElement(child, clonedProps) : child;
          })
        : children}
    </BaseText>
  );
};

Text.displayName = 'Text';

Text.propTypes = {
  ...PrimitivePropTypes,
  action: PropTypes.bool,
  align: PropTypes.oneOf(Object.values(ALIGN)),
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  displayName: PropTypes.string,
  ellipsizeMode: PropTypes.bool,
  headline: PropTypes.bool,
  hidden: PropTypes.bool,
  level: PropTypes.number,
  lineThrough: PropTypes.bool,
  role: PropTypes.oneOf(TEXT_ROLES),
  selectable: PropTypes.bool,
  small: PropTypes.bool,
  underlined: PropTypes.bool,
};

export { Text };
