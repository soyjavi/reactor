import StyleSheet from 'react-native-extended-stylesheet';

export const style = StyleSheet.create({
  hidden: {
    display: 'none',
  },

  lineThrough: {
    textDecorationLine: 'line-through',
  },

  underlined: {
    textDecorationLine: 'underline',
  },

  // TODO: Should use hook and dont create a new style
  left: {
    alignSelf: 'flex-start',
  },

  center: {
    alignSelf: 'center',
  },

  right: {
    alignSelf: 'flex-end',
  },
});
