import PropTypes from 'prop-types';
import React, { useEffect, useRef } from 'react';
import { Animated, Pressable } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

import { testID } from '@helpers';
import { styles, useBanProps, useBanStylerProps, useDevice, useStyler } from '@hooks';

import { PrimitivePropTypes } from '../Primitive.definition';
import { BANNED_PROPS, SWITCH_VARIANT } from './Switch.definition';
import { style } from './Switch.style';

const Switch = ({ checked, disabled, variant = SWITCH_VARIANT.CHECKBOX, onChange, ...others }) => {
  const bgColorAnimatedValue = useRef(new Animated.Value(disabled || !checked ? 0 : 100)).current;
  const borderColorAnimatedValue = useRef(new Animated.Value(disabled ? -100 : checked ? 100 : 0)).current;

  useEffect(() => {
    const animation = Animated.parallel([
      Animated.timing(bgColorAnimatedValue, {
        toValue: disabled || !checked ? 0 : 100,
        duration: StyleSheet.value('$motionExpand'),
        useNativeDriver: false,
      }),
      Animated.timing(borderColorAnimatedValue, {
        toValue: disabled ? -100 : checked ? 100 : 0,
        duration: StyleSheet.value('$motionExpand'),
        useNativeDriver: false,
      }),
    ]);
    animation.start();

    return animation.stop;
  }, [bgColorAnimatedValue, borderColorAnimatedValue, checked, disabled]);

  const handlePress = () => {
    onChange(!checked);
  };

  return React.createElement(onChange ? Animated.createAnimatedComponent(Pressable) : Animated.View, {
    ...useBanProps(useBanStylerProps(others), BANNED_PROPS),
    ...useStyler(
      {
        style: styles(
          style.container,
          style[variant],
          {
            backgroundColor: bgColorAnimatedValue.interpolate({
              inputRange: [0, 100],
              outputRange: [StyleSheet.value('$colorBase'), StyleSheet.value('$colorSecondary')],
            }),
            borderColor: borderColorAnimatedValue.interpolate({
              inputRange: [-100, 0, 100],
              outputRange: [
                StyleSheet.value('$colorLighten'),
                StyleSheet.value('$colorLighten'),
                StyleSheet.value('$colorSecondary'),
              ],
            }),
          },
          others.style,
        ),
        ...others,
      },
      Switch.displayName,
      useDevice(),
    ),
    ...testID(others.testID),
    accessibilityRole: variant,
    disabled,
    onPress: onChange ? handlePress : undefined,
  });
};

Switch.displayName = 'Switch';

Switch.propTypes = {
  ...PrimitivePropTypes,
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
  id: PropTypes.string,
  name: PropTypes.string,
  variant: PropTypes.oneOf(Object.values(SWITCH_VARIANT)),
  value: PropTypes.string,
  onChange: PropTypes.func,
};

export { Switch };
