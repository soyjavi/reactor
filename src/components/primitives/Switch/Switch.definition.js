const BANNED_PROPS = ['aria-describedby', 'id', 'name', 'style', 'value'];

const SWITCH_VARIANT = {
  CHECKBOX: 'checkbox',
  RADIO: 'radio',
};

export { BANNED_PROPS, SWITCH_VARIANT };
