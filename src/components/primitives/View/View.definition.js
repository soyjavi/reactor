const BANNED_PROPS = [
  'aria-controls',
  'aria-describedby',
  'aria-disabled',
  'aria-expanded',
  'aria-labelledby',
  'aria-modal',
  'aria-valuemax',
  'aria-valuemin',
  'aria-valuenow',
  'as',
  'disabled',
  'format',
  'formatValue',
  'htmlFor',
  'id',
  'onKeyUp',
  'style',
  'tabIndex',
];

const VIEW_ROLES = [
  'div',
  'article',
  'button',
  'section',
  'aside',
  'nav',
  'header',
  'footer',
  'figure',
  'ul',
  'ol',
  'li',
  'table',
  'thead',
  'tbody',
  'tr',
  'th',
  'td',
  'form',
];

export { BANNED_PROPS, VIEW_ROLES };
