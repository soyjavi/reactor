import PropTypes from 'prop-types';
import React from 'react';
import { Platform, View as NativeView } from 'react-native';

import { testID } from '@helpers';
import { useBanProps, useBanStylerProps, useDevice, useStyler } from '@hooks';

import { PrimitivePropTypes } from '../Primitive.definition';
import { BANNED_PROPS, VIEW_ROLES } from './View.definition';

const View = React.forwardRef(({ children, displayName = 'View', role, ...others }, ref) => (
  <NativeView
    {...useBanProps(useBanStylerProps(others), BANNED_PROPS)}
    {...useStyler(others, displayName, useDevice())}
    {...testID(others.testID)}
    accessibilityRole={Platform.OS === 'web' ? role : undefined}
    pointerEvents={others.pointerEvents || others.pointer}
    ref={ref}
  >
    {children}
  </NativeView>
));

View.displayName = 'View';

View.propTypes = {
  ...PrimitivePropTypes,
  children: PropTypes.node,
  displayName: PropTypes.string,
  role: PropTypes.oneOf(VIEW_ROLES),
};

export { View };
