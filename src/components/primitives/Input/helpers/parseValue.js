import { TYPE_NUMBER } from '../Input.definition';

export const parseValue = (value, type, { trimSpaces = false } = {}) =>
  value && value.toString().trim().length > 0
    ? type !== TYPE_NUMBER
      ? (trimSpaces ? value.trim() : value).toString()
      : !isNaN(value)
      ? parseFloat(value, 10)
      : undefined
    : undefined;
