import PropTypes from 'prop-types';
import React, { useImperativeHandle, useRef, useState } from 'react';
import { Platform, TextInput } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

import { getInputErrors, stylesheetFont, testID } from '@helpers';
import { styles, useBanProps, useSupportedProps, useBanStylerProps, useStyler, useDevice } from '@hooks';

import { PrimitivePropTypes } from '../Primitive.definition';
import { TEXT_STYLE } from '../Text';
import { parseValue } from './helpers';
import { MULTILINE_MIN_HEIGHT } from './Input.definition';
import { BANNED_PROPS, INPUT_TYPES, KEYBOARD_TYPES, TEXT_CONTENT_TYPES, TYPE_NUMBER } from './Input.definition';
import { style } from './Input.style';

const Input = React.forwardRef(
  (
    {
      disabled,
      multiline,
      placeholder,
      placeholderColor = StyleSheet.value('$colorLighten'),
      role,
      type = 'text',
      onChange,
      onError,
      ...others
    },
    ref,
  ) => {
    const [contentHeight, setContentHeight] = useState(MULTILINE_MIN_HEIGHT);

    const inputRef = useRef(ref);

    const isNumber = type === TYPE_NUMBER;

    useImperativeHandle(ref, () => inputRef.current);

    const handleChangeText = (next = '') => {
      if (isNumber && isNaN(next)) return;
      const value = parseValue(next, type, others);

      onError && onError(getInputErrors({ ...others, type, value }));
      onChange && onChange(value || '');
    };

    const handleContentSizeChange = ({ nativeEvent: { contentSize: { height } = {} } = {} }) => {
      if (height > MULTILINE_MIN_HEIGHT) setContentHeight(height);
    };

    return (
      <TextInput
        {...useBanProps(useBanStylerProps(others), BANNED_PROPS)}
        {...testID(others.testID, 'input')}
        style={styles(
          stylesheetFont({ nameSpace: '$fontInput' }).font,
          style.input,
          disabled && style.disabled,
          multiline && style.multiline,
          multiline && Platform.OS === 'web' && { minHeight: contentHeight },
          ...useStyler(useSupportedProps(others, TEXT_STYLE), Input.displayName, useDevice()).style,
          others.style,
        )}
        // ! TODO: find a better solution for accessibilityLabel web
        // accessibilityLabel={Platform.OS === 'web' ? others.label || others.accessibilityLabel : undefined}
        accessibilityRole={Platform.OS === 'web' ? role : undefined}
        allowFontScaling={false}
        autoCapitalize="none"
        autoCorrect={true}
        editable={!disabled}
        keyboardType={KEYBOARD_TYPES[type] || KEYBOARD_TYPES.TEXT}
        multiline={multiline}
        placeholder={placeholder}
        placeholderTextColor={placeholderColor}
        ref={inputRef}
        secureTextEntry={type === 'password'}
        textContentType={TEXT_CONTENT_TYPES[type]}
        onChangeText={handleChangeText}
        onContentSizeChange={multiline && Platform.OS === 'web' ? handleContentSizeChange : undefined}
      />
    );
  },
);

Input.displayName = 'Input';

Input.propTypes = {
  ...PrimitivePropTypes,
  defaultValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  disabled: PropTypes.bool,
  id: PropTypes.string,
  multiline: PropTypes.bool,
  name: PropTypes.string,
  placeholder: PropTypes.string,
  placeholderColor: PropTypes.string,
  required: PropTypes.bool,
  role: PropTypes.string,
  trimSpaces: PropTypes.bool,
  type: PropTypes.oneOf(INPUT_TYPES),
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  onError: PropTypes.func,
  onFocus: PropTypes.func,
};

export { Input };
