import { Platform } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

import { MULTILINE_MIN_HEIGHT } from './Input.definition';

export const style = StyleSheet.create({
  input: {
    color: '$colorContent',
    flex: 0,
    minHeight: '$inputSize',
    height: '$inputSize',
    paddingTop: '$fontInputPaddingTop',
    paddingRight: '$fontInputPaddingRight',
    paddingBottom: '$fontInputPaddingBottom',
    paddingLeft: '$fontInputPaddingLeft',
    textAlignVertical: 'center',
    ...Platform.select({
      web: {
        outlineWidth: 0,
      },
    }),
  },

  disabled: {
    color: '$colorLighten',
  },

  multiline: {
    height: 'auto',
    minHeight: MULTILINE_MIN_HEIGHT,
    paddingTop: '$spaceM + $fontInputPaddingTop',
    paddingBottom: '$spaceM',
    textAlignVertical: 'top',
  },
});
