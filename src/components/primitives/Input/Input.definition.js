import { Theme } from '@theming';

const { layoutXL, spaceM } = Theme.get();

const BANNED_PROPS = ['autoComplete', 'id', 'required', 'style'];

const MULTILINE_MIN_HEIGHT = layoutXL + spaceM;

const TYPE_NUMBER = 'number';

const INPUT_TYPES = ['text', 'password', 'email', 'date', TYPE_NUMBER, 'search', 'tel', 'time', 'url'];

const KEYBOARD_TYPES = {
  email: 'email-address',
  number: 'numeric',
  tel: 'number-pad',
  text: 'default',
  url: 'url',
};

const TEXT_CONTENT_TYPES = {
  email: 'emailAddress',
  password: 'password',
  tel: 'telephoneNumber',
  url: 'URL',
};

export { BANNED_PROPS, INPUT_TYPES, KEYBOARD_TYPES, MULTILINE_MIN_HEIGHT, TYPE_NUMBER, TEXT_CONTENT_TYPES };
