import PropTypes from 'prop-types';
import React, { useEffect, useImperativeHandle, useRef } from 'react';
import { Platform, ScrollView as BaseScrollView } from 'react-native';

import { IS_JEST, testID } from '@helpers';
import { styles, useBanProps, useBanStylerProps, useDevice, useStyler, useSupportedProps } from '@hooks';

import { PrimitivePropTypes } from '../Primitive.definition';
import { VIEW_ROLES } from '../View';
import { BANNED_PROPS, STYLE_BANNED_PROPS } from './ScrollView.definition';

const ScrollView = React.forwardRef(
  (
    {
      animated = true,
      children,
      height,
      scrollIndicator = false,
      horizontal,
      role,
      scrollEventThrottle = Platform.OS === 'ios' ? 0 : undefined,
      scrollTo,
      scrollToEnd,
      snapInterval,
      width,
      onScroll,
      ...others
    },
    forwardRef,
  ) => {
    // TODO: Understand what they were trying to do. --------------------------
    const ref = useRef();
    useImperativeHandle(forwardRef, () => ref.current);
    // -------------------------------------------------------------------------

    useEffect(() => {
      const { current } = ref;

      if (current && (scrollTo !== undefined || scrollToEnd)) {
        if (IS_JEST && Platform.OS === 'web') return;

        if (scrollToEnd) current.scrollToEnd({ animated });
        else current.scrollTo({ [horizontal ? 'x' : 'y']: scrollTo, animated });
      }
    }, [animated, horizontal, scrollTo, scrollToEnd, ref]);

    const handleScroll = (event = {}) => {
      const {
        layoutMeasurement: { height: offsetHeight, width: offsetWidth } = {},
        contentOffset: { x: X, y: Y } = {},
        contentSize: { height: scrollHeight, width: scrollWidth } = {},
      } = event.nativeEvent || {};

      const percentX = X > 0 ? Math.floor((X * 100) / (scrollWidth - offsetWidth)) : 0;
      const percentY = Y > 0 ? Math.floor((Y * 100) / (scrollHeight - offsetHeight)) : 0;

      if (
        snapInterval === undefined ||
        percentX === 100 ||
        percentY === 100 ||
        parseInt((horizontal ? X : Y) % snapInterval, 10) === 0
      )
        onScroll({ X, Y, percentX, percentY }, event);
    };

    return (
      <BaseScrollView
        {...useBanProps(useBanStylerProps(others), BANNED_PROPS)}
        {...useStyler(
          {
            ...useBanProps(others, STYLE_BANNED_PROPS),
            style: styles({ height, maxHeight: height, maxWidth: width, width }, others.style),
          },
          ScrollView.displayName,
          useDevice(),
        )}
        {...testID(others.testID)}
        {...(snapInterval
          ? {
              decelerationRate: 'fast',
              pagingEnabled: Platform.OS === 'web' || (Platform.OS === 'android' && horizontal),
              snapToAlignment: 'start',
              snapToInterval: snapInterval,
            }
          : {})}
        contentContainerStyle={
          useStyler(
            { ...useSupportedProps(others, STYLE_BANNED_PROPS), style: others.contentContainerStyle },
            ScrollView.displayName,
            useDevice(),
          ).style
        }
        accessibilityRole={Platform.OS === 'web' ? role : undefined}
        horizontal={horizontal}
        ref={ref}
        scrollEventThrottle={scrollEventThrottle}
        showsHorizontalScrollIndicator={scrollIndicator}
        showsVerticalScrollIndicator={scrollIndicator}
        onScroll={onScroll ? handleScroll : undefined}
      >
        {children}
      </BaseScrollView>
    );
  },
);

ScrollView.displayName = 'ScrollView';

ScrollView.propTypes = {
  ...PrimitivePropTypes,
  animated: PropTypes.bool,
  children: PropTypes.node,
  height: PropTypes.number,
  horizontal: PropTypes.bool,
  role: PropTypes.oneOf(VIEW_ROLES),
  scrollEventThrottle: PropTypes.number,
  scrollIndicator: PropTypes.bool,
  scrollTo: PropTypes.number,
  scrollToEnd: PropTypes.bool,
  snapInterval: PropTypes.number,
  width: PropTypes.number,
  onScroll: PropTypes.func,
};

export { ScrollView };
