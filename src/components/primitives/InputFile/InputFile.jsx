import * as DocumentPicker from 'expo-document-picker';
import * as ImagePicker from 'expo-image-picker';
import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import { Platform, Pressable } from 'react-native';

import { testID } from '@helpers';
import { DISPLAY, useBanProps, useBanStylerProps, useDevice, useStyler } from '@hooks';

import { PrimitivePropTypes } from '../Primitive.definition';
import { BANNED_PROPS } from './InputFile.definition';

const InputFile = ({
  accept,
  children,
  disabled,
  forwardedRef,
  hidden = false,
  multiple = false,
  onChange,
  onError = () => {},
  ...others
}) => {
  useEffect(() => {
    if (forwardedRef) {
      forwardedRef.current = { ...forwardedRef.current, press: handlePress };
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [forwardedRef]);

  const handlePress = async () => {
    let files;

    // TODO: Should define the common interface for "files"
    if (Platform.OS === 'web' || accept !== 'image/*') {
      // https://docs.expo.io/versions/latest/sdk/document-picker
      const { type, ...rest } = await DocumentPicker.getDocumentAsync({
        copyToCacheDirectory: false,
        type: accept,
        multiple: Platform.OS === 'web' ? multiple : false,
      });
      if (type !== 'cancel') files = rest;
    } else {
      // https://docs.expo.io/versions/latest/sdk/imagepicker/
      const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
      if (status !== 'granted') {
        onError(new Error('Sorry, we need camera roll permissions to make this work!'));
      } else {
        const { cancelled, ...rest } = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: false,
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
        });
        if (!cancelled) files = rest;
      }
    }

    if (files) onChange(files);
  };

  return (
    <Pressable
      {...useBanProps(useBanStylerProps(others), BANNED_PROPS)}
      {...useStyler({ ...others, display: hidden ? DISPLAY.NONE : others.display }, InputFile.displayName, useDevice())}
      {...testID(others.testID)}
      onPress={!disabled && onChange ? handlePress : undefined}
    >
      {children}
    </Pressable>
  );
};

InputFile.displayName = 'InputFile';

InputFile.propTypes = {
  ...PrimitivePropTypes,
  accept: PropTypes.string,
  children: PropTypes.node,
  disabled: PropTypes.bool,
  forwardedRef: PropTypes.any,
  hidden: PropTypes.bool,
  multiple: PropTypes.bool,
  onChange: PropTypes.func,
  onError: PropTypes.func,
};

export { InputFile };
