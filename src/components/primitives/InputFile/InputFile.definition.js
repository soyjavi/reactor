const BANNED_PROPS = ['id', 'style', 'label', 'capture'];

export { BANNED_PROPS };
