import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { Platform, Pressable } from 'react-native';

import { testID } from '@helpers';
import {
  // Helpers
  ALIGN,
  COLOR,
  POINTER,
  // Hooks
  useBanStylerProps,
  useDevice,
  useKeyboard,
  useStyler,
  styles,
} from '@hooks';
import { Theme } from '@theming';

// ! TODO: A primitive should not instantiate another primitive ----------------
import { Motion, MOTION_TIMING } from '../Motion';
// ! ---------------------------------------------------------------------------
import { PrimitivePropTypes } from '../Primitive.definition';
import { ROLES } from './Touchable.definition';
import { style } from './Touchable.style';

const Touchable = ({ children, feedback = {}, role = ROLES[0], onPress, onPressIn, ...others }) => {
  const { isKeyboardOpen, closeKeyboard } = useKeyboard();

  const { motionExpand } = Theme.get();

  const [layout, setLayout] = useState();
  const [wave, setWave] = useState();

  useEffect(() => {
    if (wave && !wave.active) {
      setWave({ ...wave, active: true });
      setTimeout(() => setWave(undefined), motionExpand * 2);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [wave]);

  const handleLayout = ({ nativeEvent: { layout: { height, width } = {} } = {} } = {}) => {
    if (height && width) setLayout({ height: parseInt(height, 10), width: parseInt(width, 10) });
  };

  const handlePress = (event) => {
    Platform.OS !== 'web' && isKeyboardOpen && closeKeyboard();
    onPress && onPress(event);
  };

  const handlePressIn = (event) => {
    if (feedback && layout) {
      const { height, width } = layout;
      const { locationX: left, locationY: top } = event.nativeEvent;
      const size = (height > width ? height : width) * 1.5;

      setWave({ active: false, left: left - size / 2, top: top - size / 2, size });
    }
    onPressIn && onPressIn(event);
  };

  return (
    <Pressable
      {...useBanStylerProps(others)}
      {...useStyler(
        {
          ...others,
          alignSelf: others.alignSelf || ALIGN.START,
          style: styles(style.container, others.style),
        },
        Touchable.displayName,
        useDevice(),
      )}
      {...testID(others.testID)}
      accessibilityRole={role}
      pointerEvents={onPress ? others.pointerEvents : POINTER.NONE}
      onLayout={onPress && feedback ? handleLayout : undefined}
      onPress={handlePress}
      onPressIn={handlePressIn}
    >
      {({ pressed }) => (
        <>
          {feedback && onPress && (
            <>
              <Motion
                backgroundColor={feedback.color || COLOR.TOUCHABLE}
                pointerEvents={POINTER.NONE}
                value={{ opacity: pressed ? feedback.opacity || 1 : 0 }}
                style={styles(style.wave, style.pressed)}
                timing={MOTION_TIMING.LINEAR}
              />
              {wave && (
                <Motion
                  backgroundColor={feedback.color || COLOR.TOUCHABLE}
                  duration={motionExpand * 2}
                  pointerEvents={POINTER.NONE}
                  style={styles(style.wave, {
                    ...wave,
                    borderRadius: wave.size / 2,
                    height: wave.size,
                    width: wave.size,
                  })}
                  timing={MOTION_TIMING.LINEAR}
                  value={{ scale: wave.active ? 1 : 0, opacity: wave.active ? 0 : feedback.opacity || 1 }}
                />
              )}
            </>
          )}
          {children}
        </>
      )}
    </Pressable>
  );
};

Touchable.displayName = 'Touchable';

Touchable.propTypes = {
  ...PrimitivePropTypes,
  children: PropTypes.node,
  feedback: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
  role: PropTypes.oneOf(ROLES),
  tabIndex: PropTypes.number,
  onPress: PropTypes.func,
  onPressIn: PropTypes.func,
};

export { Touchable };
