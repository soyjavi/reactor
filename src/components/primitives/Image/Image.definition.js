const BANNED_PROPS = ['id', 'style'];

const IMAGE_RESIZE_MODES = ['contain', 'cover', 'stretch', 'repeat', 'center'];

export { BANNED_PROPS, IMAGE_RESIZE_MODES };
