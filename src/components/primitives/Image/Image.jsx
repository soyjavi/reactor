import PropTypes from 'prop-types';
import React, { useLayoutEffect, useMemo, useState } from 'react';
import { Image as BaseImage } from 'react-native';

import { testID } from '@helpers';
import { styles, useBanProps, useBanStylerProps, useDevice, useStyler } from '@hooks';

import { PrimitivePropTypes } from '../Primitive.definition';
import { getUri } from './helpers';
import { BANNED_PROPS, IMAGE_RESIZE_MODES } from './Image.definition';

const Image = ({ alt, autoSize = false, resizeMode = IMAGE_RESIZE_MODES[0], src, srcSet, ...others }) => {
  const { uri, sizeUri } = useMemo(() => getUri(src, srcSet), [src, srcSet]);

  const [size, setSize] = useState({ height: undefined, width: undefined });

  useLayoutEffect(() => {
    if (autoSize) {
      BaseImage.getSize(sizeUri, (width, height) => height && width && setSize({ height, width }));
    }
  }, [autoSize, others.style, sizeUri]);

  const { height, width } = size;

  return (
    <BaseImage
      {...useBanProps(useBanStylerProps(others), BANNED_PROPS)}
      {...useStyler(
        {
          ...others,
          style: styles(
            autoSize && height && width ? { height, width, aspectRatio: width / height } : undefined,
            others.style,
          ),
        },
        Image.displayName,
        useDevice(),
      )}
      {...testID(others.testID)}
      resizeMode={resizeMode}
      accessibilityLabel={alt || others.accessibilityLabel}
      source={uri ? { uri } : src}
    />
  );
};

Image.displayName = 'Image';

Image.propTypes = {
  ...PrimitivePropTypes,
  alt: PropTypes.string,
  autoSize: PropTypes.bool,
  resizeMode: PropTypes.oneOf(IMAGE_RESIZE_MODES),
  src: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  srcSet: PropTypes.string,
  onError: PropTypes.func,
  onLoad: PropTypes.func,
};

export { Image };
