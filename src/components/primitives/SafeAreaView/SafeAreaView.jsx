import PropTypes from 'prop-types';
import React from 'react';
import { SafeAreaView as NativeSafeAreaView } from 'react-native';

import { testID } from '@helpers';
import { useBanStylerProps, useStyler } from '@hooks';

import { PrimitivePropTypes } from '../Primitive.definition';

const SafeAreaView = ({ children, ...others }) => (
  <NativeSafeAreaView {...useBanStylerProps(others)} {...useStyler(others)} {...testID(others.testID)}>
    {children}
  </NativeSafeAreaView>
);

SafeAreaView.displayName = 'SafeAreaView';

SafeAreaView.propTypes = {
  ...PrimitivePropTypes,
  children: PropTypes.node.isRequired,
};

export { SafeAreaView };
