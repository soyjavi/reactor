import { Picker } from '@react-native-picker/picker';
import PropTypes from 'prop-types';
import React from 'react';
import { Platform } from 'react-native';

import { stylesheetFont, testID } from '@helpers';
import { styles, useBanStylerProps, useDevice, useStyler } from '@hooks';

import { PrimitivePropTypes } from '../Primitive.definition';
import { style } from './Select.style';
import { SelectIOS } from './SelectIOS';

const Select = ({ disabled, emptyOptionText: emptyOption, id, options = [], value, onChange, ...others }) => {
  const handleChange = (next) => {
    const value = next === emptyOption ? undefined : next;

    onChange && onChange(value);
    others.onBlur && others.onBlur();
  };

  const styleFont = stylesheetFont({ nameSpace: '$fontInput' }).font;

  const children = [];
  if (emptyOption) children.push(<Picker.Item key={emptyOption} label={emptyOption} value={emptyOption} />);
  options.forEach((option = {}) => {
    const { name, value } = typeof option === 'object' ? option : { name: option, value: option };

    children.push(<Picker.Item key={value} label={name} value={value} />);
  });

  return React.createElement(
    Platform.OS === 'ios' ? SelectIOS : Picker,
    {
      ...useBanStylerProps(others),
      ...useStyler(
        {
          ...others,
          style: styles(styleFont, style.container, disabled && style.disabled, others.style),
        },
        Select.displayName,
        useDevice(),
      ),
      ...testID(others.testID),
      id,
      enabled: !disabled,
      itemStyle: [styleFont, style.item],
      selectedValue: value,
      onValueChange: handleChange,
    },
    children,
  );
};

Select.propTypes = {
  ...PrimitivePropTypes,
  disabled: PropTypes.bool,
  id: PropTypes.string,
  emptyOptionText: PropTypes.string,
  options: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string,
        value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      }),
    ),
  ]),
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  onFocus: PropTypes.func,
};

export { Select };
