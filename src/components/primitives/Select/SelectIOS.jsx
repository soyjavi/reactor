import { PickerIOS } from '@react-native-picker/picker';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { Modal, Pressable, Text, View } from 'react-native';

import { testID } from '@helpers';
import { POINTER } from '@hooks';

import { Icon } from '../Icon';
import { PrimitivePropTypes } from '../Primitive.definition';
import { style } from './Select.style';

const SelectIOS = ({ children, enabled, id, itemStyle, selectedValue, onValueChange, ...others }) => {
  const [modal, setModal] = useState(false);

  const option = selectedValue
    ? children.find(({ props: { value = '' } }) => value.toString() === selectedValue.toString())
    : undefined;
  const { props: { label = '' } = {} } = option || children[0] || {};

  return (
    <>
      <Pressable {...others} disabled={!enabled} onPress={() => setModal(true)}>
        <Text pointerEvents={POINTER.NONE} style={[itemStyle, style.itemIOS]}>
          {label}
        </Text>
      </Pressable>
      <Modal animationType="slide" transparent visible={modal} onRequestClose={() => setModal(false)}>
        <View style={style.containerIOS}>
          <Pressable style={style.pressableIconIOS} onPress={() => setModal(false)} {...testID('select-button-close')}>
            <Icon name="close" />
          </Pressable>
          <PickerIOS {...{ id, enabled, itemStyle, selectedValue, onValueChange }}>{children}</PickerIOS>
        </View>
      </Modal>
    </>
  );
};

SelectIOS.propTypes = {
  ...PrimitivePropTypes,
  children: PropTypes.arrayOf(PropTypes.node),
  enabled: PropTypes.bool,
  id: PropTypes.string,
  itemStyle: PropTypes.any,
  selectedValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onValueChange: PropTypes.func,
};

export { SelectIOS };
