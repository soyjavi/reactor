import PropTypes from 'prop-types';
import React, { useEffect, useLayoutEffect, useState } from 'react';
import { Animated, Platform } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

import { capitalize, IS_JEST, testID } from '@helpers';
import { useBanProps, useBanStylerProps, useDevice, useStyler, styles } from '@hooks';

import { PrimitivePropTypes } from '../Primitive.definition';
import { VIEW_ROLES } from '../View';
import { buildStyle, calcEasing, calcState, toAnimatedValue, getNativeDriver } from './helpers';
import { BANNED_PROPS, MOTION_TIMING, MOTION_TYPES } from './Motion.definition';

const Motion = React.forwardRef(
  (
    {
      children,
      delay = false,
      disabled,
      forceFinish = false,
      layout,
      loops = 1,
      role,
      timing = MOTION_TIMING.QUAD,
      type = MOTION_TYPES.EXPAND,
      value = {},
      onCancel = () => {},
      onFinish = () => {},
      ...others
    },
    ref,
  ) => {
    const [active, setActive] = useState(false);
    const [mount, setMount] = useState(false);
    const [state, setState] = useState(calcState({ value, layout }));

    useLayoutEffect(() => {
      setMount(true);
    }, []);

    useEffect(() => {
      if (!mount || (forceFinish && active)) return;

      if (JSON.stringify(Object.keys(value).sort()) !== JSON.stringify(Object.keys(state).sort())) {
        setState(() => {
          const nextState = calcState({ layout, state, value });
          animate(nextState);

          return nextState;
        });
      } else {
        animate(state);
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [delay, loops, timing, type, value]);

    const animate = (state) => {
      if (IS_JEST) return;

      const duration = others.duration || StyleSheet.value(`$motion${capitalize(type)}`);
      const keys = Object.keys(value);

      const useNativeDriver = getNativeDriver(keys);

      const animations = Animated.parallel(
        keys
          .filter((key) => state[key] !== undefined)
          .map((key) =>
            Animated.timing(state[key], {
              toValue: toAnimatedValue(key, value[key], layout),
              delay: delay ? duration : 0,
              duration: disabled ? 0 : duration,
              easing: disabled ? undefined : calcEasing(timing),
              useNativeDriver,
            }),
          ),
      );

      const callbacks = ({ finished }) => {
        if (!finished) {
          animations.stop();
          onCancel();
        } else {
          onFinish();
        }

        if (forceFinish && !disabled) setActive(false);
      };

      if (loops === 1) animations.start(callbacks);
      else Animated.loop(animations, { iterations: loops }).start(callbacks);

      if (forceFinish && !disabled) setActive(true);
    };

    return (
      <Animated.View
        {...useBanProps(useBanStylerProps(others), BANNED_PROPS)}
        {...testID(others.testID)}
        accessibilityRole={Platform.OS === 'web' ? role : undefined}
        pointerEvents={others.pointerEvents || others.pointer}
        ref={ref}
        style={styles(
          ...useStyler(others, Motion.displayName, useDevice()).style,
          buildStyle(value, disabled || IS_JEST ? value : state),
        )}
      >
        {children}
      </Animated.View>
    );
  },
);

Motion.propTypes = {
  ...PrimitivePropTypes,
  children: PropTypes.node,
  delay: PropTypes.bool,
  disabled: PropTypes.bool,
  duration: PropTypes.number,
  forceFinish: PropTypes.bool,
  layout: PropTypes.shape({
    height: PropTypes.number,
    left: PropTypes.number,
    top: PropTypes.number,
    width: PropTypes.number,
    x: PropTypes.number,
    y: PropTypes.number,
  }),
  loops: PropTypes.number,
  role: PropTypes.oneOf(VIEW_ROLES),
  timing: PropTypes.oneOf(Object.values(MOTION_TIMING)),
  type: PropTypes.oneOf(Object.values(MOTION_TYPES)),
  value: PropTypes.shape({}),
  onCancel: PropTypes.func,
  onFinish: PropTypes.func,
};

export { Motion };
