import { Easing } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';

// ! TODO: Should remove this helper when we use an 100% isomorphic testing environment.
export const calcEasing = (timing) =>
  StyleSheet.value(`$motionEasing`)[timing] ||
  (Easing && Easing.inOut && Easing.quad ? Easing.inOut(Easing.quad) : undefined);
