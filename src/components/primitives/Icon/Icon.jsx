import PropTypes from 'prop-types';
import React from 'react';
import { Text } from 'react-native';

import { stylesheetColor, testID } from '@helpers';
import { styles, useBanProps, useBanStylerProps, useDevice, useStyler } from '@hooks';
import { Theme } from '@theming';

import { PrimitivePropTypes } from '../Primitive.definition';
import { BANNED_PROPS } from './Icon.definition';
import { style } from './Icon.style';

const Icon = ({ color, name = 'reactor', ...others }) => (
  <Text
    {...useBanProps(useBanStylerProps(others), BANNED_PROPS)}
    {...useStyler(
      {
        ...others,
        style: styles(style.icon, stylesheetColor(color).color, others.style),
      },
      Icon.displayName,
      useDevice(),
    )}
    {...testID(others.testID)}
    accessibilityElementsHidden={true}
    allowFontScaling={false}
    importantForAccessibility="no"
    selectable={false}
  >
    {String.fromCharCode((Theme.get('iconGlyphs') || {})[name])}
  </Text>
);

Icon.propTypes = {
  ...PrimitivePropTypes,
  color: PropTypes.string,
  name: PropTypes.string.isRequired,
};

Icon.displayName = 'Icon';

export { Icon };
