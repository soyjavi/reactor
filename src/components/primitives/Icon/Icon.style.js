import StyleSheet from 'react-native-extended-stylesheet';

export const style = StyleSheet.create({
  icon: {
    fontFamily: '$iconFamily',
    fontSize: '$iconSize',
    fontStyle: 'normal',
    fontWeight: 'normal',
    height: '$iconSize',
    minHeight: '$iconSize',
    minWidth: '$iconSize',
    width: '$iconSize',
  },
});
