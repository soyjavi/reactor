import PropTypes from 'prop-types';
import React, { useEffect, useRef } from 'react';

import { Thumbnail } from '@atoms';
import { KEYBOARD } from '@helpers';
import { styles } from '@hooks';
import { InputFile, Text, Touchable } from '@primitives';

const InputImage = ({ height, icon = 'expand', id, label, src, width, onChange, ...others }) => {
  const refInputFile = useRef(null);

  useEffect(() => {
    refInputFile.current.value = '';
  }, [src]);

  const handleKeyUp = (event) => {
    if (event.keyCode === KEYBOARD.ESPACE || event.keyCode === KEYBOARD.RETURN) refInputFile.current.press();
    else event.preventDefault();
  };

  const handlePress = () => {
    refInputFile.current.press();
  };

  return (
    <Touchable {...others} aria-controls={id} onKeyUp={handleKeyUp} onPress={handlePress}>
      <Thumbnail {...{ height, icon, width, style: styles(others.style, others.customStyle) }} src={src} />
      {label && (
        <Text role="label" hidden>
          {label}
        </Text>
      )}
      <InputFile accept="image/*" id={id} forwardedRef={refInputFile} hidden onChange={onChange} />
    </Touchable>
  );
};

InputImage.displayName = 'InputImage';

InputImage.propTypes = {
  height: PropTypes.number,
  icon: PropTypes.string,
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  src: PropTypes.string,
  width: PropTypes.number,
  onChange: PropTypes.func,
};

export { InputImage };
