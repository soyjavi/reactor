import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useState } from 'react';
import { KeyboardAvoidingView, Modal as NativeModal, Platform } from 'react-native';

import { Box, Button, Layout, Overlay } from '@atoms';
import { testID } from '@helpers';
import { ALIGN, COLOR, FLEX_DIRECTION, POINTER, POSITION, SPACE, styles, useDevice, useSwipe, useBack } from '@hooks';
import { Motion, MOTION_TIMING, SafeAreaView, Text, View } from '@primitives';
import { Theme } from '@theming';

import { style } from './Modal.style';

const IS_JEST = process.env.JEST_WORKER_ID !== undefined;

const MIN_DELTA_TO_CLOSE = 80;

const Modal = React.forwardRef(
  (
    {
      children,
      color = COLOR.BASE,
      contentStyle,
      description,
      isVisible = false,
      overlayClose = true,
      size = { M: '2/3', L: '1/3' },
      swipeable = false,
      title,
      onClose,
      onHardwareBackPress,
      ...others
    },
    ref,
  ) => {
    const { screen } = useDevice();
    const handleSwipe = useSwipe({
      onSwiping: ({ deltaY, down } = {}) => {
        const nextTop = Math.abs(parseInt(deltaY, 10));

        if (!down || nextTop === 0) return;
        setTop(nextTop);
        setSwiping(true);
      },
      onSwiped: ({ deltaY, down } = {}) => {
        if (!down) return;

        setTop(0);
        if (onClose && Math.abs(deltaY) >= MIN_DELTA_TO_CLOSE) onClose();
        else setTimeout(() => setSwiping(false), motionCollapse);
      },
    });

    useBack(
      useCallback(() => {
        if (isVisible) {
          if (onHardwareBackPress) onHardwareBackPress();
          else if (onClose) onClose();
        }

        return isVisible;
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, [isVisible, onClose]),
    );

    const { motionCollapse, spaceXXL } = Theme.get();

    const [active, setActive] = useState(false);
    const [top, setTop] = useState(0);
    const [swiping, setSwiping] = useState(false);

    useEffect(() => {
      setSwiping(false);
      const timer = setTimeout(() => setActive(isVisible), isVisible ? 0 : motionCollapse);
      return () => clearTimeout(timer);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isVisible]);

    const colorContent = color === COLOR.CONTENT ? COLOR.BASE : undefined;
    const isSwipeable = screen.S && swipeable;

    const maxHeight = screen.height - (!screen.S ? spaceXXL * 2 : 0);
    const textStyle = { align: ALIGN.CENTER, color: colorContent };
    const visible = {
      modal: isVisible || active,
      children: active && isVisible === false ? false : active,
    };

    return React.createElement(
      !IS_JEST ? NativeModal : React.Fragment,
      !IS_JEST ? { transparent: true, visible: visible.modal, onRequestClose: onClose } : {},
      <View
        {...others}
        {...(!visible.modal
          ? {
              accessibilityElementsHidden: true,
              importantForAccessibility: 'no-hide-descendants',
              pointer: POINTER.NONE,
            }
          : undefined)}
        alignItems={screen.S ? ALIGN.END : ALIGN.CENTER}
        flexDirection={FLEX_DIRECTION.ROW}
        layer={SPACE.M}
        position={POSITION.FIXED}
        role="aside"
        style={styles(style.modal, others.style)}
        wide
      >
        <Overlay isVisible={visible.children} onPress={onClose && overlayClose ? onClose : undefined} />
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          enabled={isVisible}
          keyboardVerticalOffset={0}
          style={style.keyboardContainer}
        >
          <Layout fullWidth={screen.S} justifyContent={ALIGN.CENTER}>
            <Motion
              disabled={top !== 0}
              layout={screen}
              position={POSITION.RELATIVE}
              role="section"
              timing={
                visible.children && ((visible.children && screen.S && swiping) || !screen.S)
                  ? MOTION_TIMING.SPRING
                  : undefined
              }
              type={visible.children ? 'expand' : 'collapse'}
              value={
                screen.S
                  ? { translateY: visible.children ? top || '0%' : '100%' }
                  : { opacity: visible.children ? 1 : 0, scale: visible.children ? 1 : 0.9 }
              }
              wide
            >
              <Box
                align={ALIGN.CENTER}
                backgroundColor={color}
                paddingTop={onClose ? SPACE.S : SPACE.XXL}
                size={size}
                style={styles(style.box, { maxHeight })}
              >
                <SafeAreaView wide>
                  {onClose && (
                    <View
                      {...(isSwipeable && visible.children ? handleSwipe : undefined)}
                      flexDirection={FLEX_DIRECTION.ROW}
                      justifyContent={isSwipeable ? ALIGN.CENTER : ALIGN.END}
                      paddingRight={!isSwipeable ? SPACE.L : undefined}
                      layer={SPACE.S}
                      wide={!isSwipeable}
                    >
                      <Button
                        color={colorContent}
                        icon={isSwipeable ? 'down' : 'close'}
                        accessibilityLabel="close"
                        style={!isSwipeable ? style.buttonClose : undefined}
                        small
                        squared
                        onPress={onClose}
                      />
                    </View>
                  )}
                  <View ref={ref} style={styles(contentStyle || style.content, { maxHeight: maxHeight - spaceXXL })}>
                    {(title || description) && (
                      <View
                        alignItems={ALIGN.CENTER}
                        role="header"
                        flexDirection={FLEX_DIRECTION.COLUMN}
                        marginTop={onClose && (title || description) ? SPACE.XL : undefined}
                        marginBottom={SPACE.L}
                      >
                        {title && (
                          <Text {...textStyle} level={1} role="h2" {...testID(others.testID, 'title')}>
                            {title}
                          </Text>
                        )}
                        {description && (
                          <Text {...textStyle} level={2} marginTop={title ? SPACE.M : undefined}>
                            {description}
                          </Text>
                        )}
                      </View>
                    )}
                    {children}
                  </View>
                </SafeAreaView>
              </Box>
            </Motion>
          </Layout>
        </KeyboardAvoidingView>
      </View>,
    );
  },
);

Modal.propTypes = {
  children: PropTypes.node,
  color: PropTypes.string,
  contentStyle: PropTypes.oneOfType([PropTypes.number, PropTypes.object, PropTypes.array]),
  description: PropTypes.string,
  isVisible: PropTypes.bool,
  overlayClose: PropTypes.bool,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
  swipeable: PropTypes.bool,
  title: PropTypes.string,
  onClose: PropTypes.func,
  onHardwareBackPress: PropTypes.func,
};

export { Modal };
