import StyleSheet from 'react-native-extended-stylesheet';

export const style = StyleSheet.create({
  box: {
    alignItems: 'center',
  },

  buttonClose: {
    marginRight: '$spaceXS * -1',
  },

  content: {
    paddingBottom: '$spaceXL',
    paddingHorizontal: '$spaceXL',
  },

  keyboardContainer: {
    flex: 1,
    width: '100%',
    zIndex: '$elevationLayerM', // ? Is this mandatory?
  },

  modal: {
    bottom: 0,
    height: '100%',
    left: 0,
    top: 0,
    right: 0,
    overflow: 'hidden',
  },
});
