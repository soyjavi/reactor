import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';

import { Button } from '@atoms';
import { ALIGN, COLOR, DISPLAY, FLEX_DIRECTION, POINTER, SPACE, styles } from '@hooks';
import { Motion, MOTION_TIMING, SafeAreaView, Text, View } from '@primitives';

import { calcMotion } from './helpers';
import { style } from './Notification.style';

const Notification = ({
  children,
  color = COLOR.SECONDARY,
  isVisible = false,
  motion = 'top',
  timeoutClose,
  text,
  timing,
  onClose,
  ...others
}) => {
  const [layout, setLayout] = useState();

  const handleLayout = ({ nativeEvent: { layout } = {} } = {}) => {
    setLayout(layout);
  };

  useEffect(() => {
    if (!timeoutClose || !onClose) return;

    let idTimeOut;
    if (isVisible) idTimeOut = setTimeout(onClose, timeoutClose);
    else clearTimeout(idTimeOut);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isVisible]);

  const colorContent = color === COLOR.CONTENT ? COLOR.BASE : undefined;

  return (
    <Motion
      layout={layout}
      pointer={!isVisible ? POINTER.NONE : undefined}
      timing={timing}
      type={isVisible ? 'expand' : 'collapse'}
      value={motion ? calcMotion({ isVisible, motion }) : undefined}
      wide
      role="aside"
      onLayout={handleLayout}
    >
      <View {...others} backgroundColor={color}>
        <SafeAreaView>
          <View style={style.notification}>
            <View flex={SPACE.XS} flexDirection={FLEX_DIRECTION.COLUMN} wide>
              {text && (
                <Text color={colorContent} display={DISPLAY.BLOCK} small>
                  {text}
                </Text>
              )}
              {children}
            </View>

            {onClose && <Button icon="close" small squared style={style.button} onPress={onClose} />}
          </View>
        </SafeAreaView>
      </View>
    </Motion>
  );
};

Notification.displayName = 'Notification';

Notification.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  color: PropTypes.string,
  isVisible: PropTypes.bool,
  motion: PropTypes.oneOf(['top', 'bottom', 'pop']),
  text: PropTypes.string,
  timeoutClose: PropTypes.number,
  timing: PropTypes.oneOf(Object.values(MOTION_TIMING)),
  onClose: PropTypes.func,
};

export { Notification };
