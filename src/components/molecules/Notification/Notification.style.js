import StyleSheet from 'react-native-extended-stylesheet';

export const style = StyleSheet.create({
  notification: {
    alignItems: 'center',
    flexDirection: 'row',
    padding: '$spaceS',
    width: 'auto',
  },

  button: {
    alignSelf: 'flex-start',
    marginTop: '$spaceS * -1',
    marginRight: '$spaceS * -1',
  },
});
