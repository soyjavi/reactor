import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { Platform } from 'react-native';

import { ButtonIcon } from '@atoms';
import { ALIGN, COLOR, FLEX_DIRECTION, POINTER, POSITION, SIZE, styles, useId } from '@hooks';
import { Motion, Text, View } from '@primitives';

import { style } from './ToolTip.style';

export const ToolTip = ({ align = 'top', color = COLOR.INFO, text, title, children, isVisible = true, ...others }) => {
  const id = useId();

  const [visible, setVisible] = useState(isVisible);

  const toggleVisible = () => {
    setVisible(!visible);
  };

  const colorContent = color === COLOR.CONTENT ? COLOR.BASE : undefined;

  return (
    <View {...others} layer={SIZE.M} position={POSITION.RELATIVE} style={styles(others.style, others.customStyle)}>
      <Motion
        id={id}
        layer={SIZE.S}
        pointer={!visible ? POINTER.NONE : undefined}
        position={POSITION.ABSOLUTE}
        role="section"
        style={styles(style.toolTip, style[align], others.style, others.customStyle)}
        tabIndex={0}
        timing="spring"
        value={{
          opacity: visible ? 1 : 0,
          translateY: visible ? `${align === 'top' ? '-' : ''}1rem` : 0,
        }}
        wide
      >
        <View backgroundColor={color} style={style.box} wide>
          {
            // ? It's not possible to touch anything outside the parent bounds in android: https://github.com/facebook/react-native/issues/27232
            !Platform.OS !== 'android' && (
              <ButtonIcon
                color={colorContent}
                layer={SIZE.XS}
                name="close"
                position={POSITION.ABSOLUTE}
                style={style.button}
                onPress={toggleVisible}
              />
            )
          }
          <View flexDirection={FLEX_DIRECTION.COLUMN} padding={SIZE.M}>
            {title && (
              <Text role="strong" align={ALIGN.CENTER} color={colorContent} detail marginBottom={SIZE.XS} upperCase>
                {title}
              </Text>
            )}
            <Text align={ALIGN.CENTER} color={colorContent} detail level={2}>
              {text}
            </Text>
          </View>
        </View>
        <View
          backgroundColor={color}
          pointer={POINTER.NONE}
          position={POSITION.ABSOLUTE}
          style={styles(style.arrow, align === ALIGN.TOP ? style.arrowTop : style.arrowBottom)}
        />
      </Motion>
      {React.Children.map(children, (child) =>
        React.cloneElement(child, {
          onPress: toggleVisible,
          'aria-describedby': id,
        }),
      )}
    </View>
  );
};

ToolTip.displayName = 'ToolTip';

ToolTip.propTypes = {
  align: PropTypes.oneOf(['top', 'bottom']),
  children: PropTypes.node.isRequired,
  color: PropTypes.string,
  isVisible: PropTypes.bool,
  text: PropTypes.string.isRequired,
  title: PropTypes.string,
};
