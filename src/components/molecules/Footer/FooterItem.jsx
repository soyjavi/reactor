import PropTypes from 'prop-types';
import React from 'react';

import { ButtonIcon } from '@atoms';
import { ALIGN, COLOR, DISPLAY, FLEX_DIRECTION, SIZE } from '@hooks';
import { Text, Touchable } from '@primitives';

import { style } from './FooterItem.style';

export const FooterItem = ({
  active,
  color = COLOR.GRAYSCALE_XL,
  colorActiveIcon = COLOR.PRIMARY,
  colorActiveText = COLOR.CONTENT,
  icon,
  onPress,
  text,
  notification,
  ...others
}) => (
  <Touchable
    {...others}
    alignItems={ALIGN.CENTER}
    flex={SIZE.XS}
    flexDirection={FLEX_DIRECTION.COLUMN}
    justifyContent={ALIGN.BETWEEN}
    paddingVertical={SIZE.S}
    onPress={onPress}
  >
    {icon && (
      <ButtonIcon
        color={active ? colorActiveIcon : color}
        colorNotification={colorActiveIcon}
        name={icon}
        notification={notification}
        style={style.iconContainer}
        small
      />
    )}
    {text && (
      <Text
        align={ALIGN.CENTER}
        color={active ? colorActiveText : color}
        detail
        display={DISPLAY.BLOCK}
        ellipsizeMode
        level={2}
        style={style.text}
      >
        {text}
      </Text>
    )}
  </Touchable>
);

FooterItem.displayName = 'FooterItem';

FooterItem.propTypes = {
  active: PropTypes.bool,
  icon: PropTypes.string,
  color: PropTypes.string,
  colorActiveIcon: PropTypes.string,
  colorActiveText: PropTypes.string,
  notification: PropTypes.bool,
  onPress: PropTypes.func,
  text: PropTypes.string,
  value: PropTypes.string.isRequired,
};
