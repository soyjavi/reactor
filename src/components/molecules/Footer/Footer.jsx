import PropTypes from 'prop-types';
import React from 'react';

import { ALIGN, COLOR, FLEX_DIRECTION, SIZE } from '@hooks';
import { SafeAreaView, View } from '@primitives';

import { FooterItem } from './FooterItem';

export const Footer = ({
  children,
  color = COLOR.BASE,
  container: Container = React.Fragment,
  onChange,
  value,
  ...others
}) => (
  <View {...others} backgroundColor={color} role="footer" wide>
    <Container>
      <SafeAreaView>
        <View
          alignItems={ALIGN.CENTER}
          flexDirection={FLEX_DIRECTION.ROW}
          justifyContent={ALIGN.BETWEEN}
          paddingHorizontal={SIZE.S}
        >
          {React.Children.map(children, (child) => (
            <FooterItem
              key={child.props.value}
              {...child.props}
              active={child.props.value === value}
              onPress={onChange ? () => onChange(child.props.value) : undefined}
            />
          ))}
        </View>
      </SafeAreaView>
    </Container>
  </View>
);

Footer.displayName = 'Footer';

Footer.propTypes = {
  children: PropTypes.node,
  color: PropTypes.string,
  container: PropTypes.any,
  onChange: PropTypes.func,
  value: PropTypes.string,
};
