import PropTypes from 'prop-types';
import React from 'react';
import { Platform, StatusBar } from 'react-native';

import { ButtonIcon } from '@atoms';
import { ALIGN, COLOR, FLEX_DIRECTION, SIZE, POSITION, styles } from '@hooks';
import { SafeAreaView, Text, View } from '@primitives';

import { style } from './Header.style';

export const Header = ({
  children,
  color = COLOR.BASE,
  container: Container = React.Fragment,
  fullWidth = true,
  logo,
  notification,
  onBack,
  onClose,
  onMenu,
  title,
  ...others
}) => {
  const colorContent = color === COLOR.CONTENT ? COLOR.BASE : undefined;

  return (
    <View {...others} backgroundColor={color} role="header" wide>
      <Container>
        <SafeAreaView style={Platform.OS === 'android' ? { paddingTop: StatusBar.currentHeight } : undefined}>
          <View
            alignItems={ALIGN.CENTER}
            alignSelf={ALIGN.CENTER}
            flexDirection={FLEX_DIRECTION.ROW}
            paddingHorizontal={SIZE.S}
            style={styles(style.header, fullWidth && style.fullWidth)}
          >
            {onBack && <ButtonIcon alignSelf={ALIGN.CENTER} color={colorContent} name="arrow-left" onPress={onBack} />}
            {logo && (
              <View position={POSITION.ABSOLUTE} style={style.logo}>
                {logo}
              </View>
            )}

            {title || onClose || onMenu ? (
              <>
                {title && (
                  <Text
                    action
                    align={ALIGN.CENTER}
                    color={colorContent}
                    position={POSITION.ABSOLUTE}
                    style={style.title}
                    upperCase
                  >
                    {title}
                  </Text>
                )}
                <View alignItems={ALIGN.CENTER} flexDirection={FLEX_DIRECTION.ROW} style={style.actions}>
                  {children}
                  {onMenu && (
                    <ButtonIcon
                      alignSelf={ALIGN.CENTER}
                      color={colorContent}
                      name="menu"
                      notification={notification}
                      onPress={onMenu}
                      testID="menu-button"
                    />
                  )}
                  {onClose && <ButtonIcon color={colorContent} name="close" onPress={onClose} />}
                </View>
              </>
            ) : (
              children
            )}
          </View>
        </SafeAreaView>
      </Container>
    </View>
  );
};

Header.displayName = 'Header';

Header.propTypes = {
  children: PropTypes.node,
  color: PropTypes.string,
  container: PropTypes.any,
  fullWidth: PropTypes.bool,
  logo: PropTypes.node,
  notification: PropTypes.bool,
  onBack: PropTypes.func,
  onClose: PropTypes.func,
  onMenu: PropTypes.func,
  title: PropTypes.string,
};
