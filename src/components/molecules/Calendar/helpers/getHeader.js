import { capitalize, toLocale } from '@helpers';

// export const capitalize = (value) => value.replace(/(\b\w)/gi, (char) => char.toUpperCase());

export const getHeader = (date, locale) => {
  const month = capitalize(toLocale(date, { locale: locale, month: 'long' }));
  const year = toLocale(date, { locale: locale, year: 'numeric' });

  return `${capitalize(month)} ${year}`;
};
