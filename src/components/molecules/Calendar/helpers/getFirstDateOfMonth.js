import { UTC } from '@helpers';

export const getFirstDateOfMonth = (date) => UTC(new Date(date.getFullYear(), date.getMonth(), 1));
