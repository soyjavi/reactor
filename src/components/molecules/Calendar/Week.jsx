import PropTypes from 'prop-types';
import React from 'react';

import { getToday, toLocale, UTC } from '@helpers';
import { ALIGN, COLOR, FLEX_DIRECTION, SPACE, styles } from '@hooks';
import { Text, Touchable, View } from '@primitives';

import { DAYS } from './Calendar.definition';
import { getFirstDateOfWeek } from './helpers';
import { style } from './Week.style';

export const Week = ({
  color,
  disabledDates = [],
  disabledPast = false,
  disabledToday = false,
  from,
  to,
  month,
  number,
  year,
  onPress,
  selected,
  ...others
}) => {
  const disabledDatesTS = disabledDates.map((date) => UTC(new Date(date)).getTime()).filter((date) => !isNaN(date));
  const firstDate = getFirstDateOfWeek(number, year);
  const todayTS = getToday().getTime();
  const fromTS = from ? UTC(new Date(from)).getTime() : undefined;
  const toTS = to ? UTC(new Date(to)).getTime() : undefined;

  return (
    <View flexDirection={FLEX_DIRECTION.ROW} marginBottom={SPACE.S}>
      {DAYS.map((day) => {
        const date = UTC(new Date(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate() + day));
        const dateTS = date.getTime();

        const isPast = disabledPast && date.getTime() < todayTS;
        const isWeekend = date.getDay() === 0 || date.getDay() === 6;
        const isDisabled =
          isPast ||
          isWeekend ||
          disabledDatesTS.includes(dateTS) ||
          (disabledToday && dateTS === todayTS) ||
          dateTS < fromTS ||
          dateTS > toTS;
        const isSelected = selected && dateTS === selected.getTime();
        const isVisible = date.getMonth() === month;
        const isTouchable = !isSelected && !isDisabled;
        const isToday = dateTS === todayTS;

        const events =
          isVisible && !isDisabled && !isSelected
            ? {
                onPress: () => onPress(date),
              }
            : undefined;

        return (
          <Touchable
            {...events}
            alignItems={ALIGN.CENTER}
            testID={events && others.testID ? `${others.testID}-${day}` : undefined}
            flex={SPACE.XS}
            justifyContent={ALIGN.CENTER}
            key={day}
            tabIndex={!isDisabled ? date.getDate() : undefined}
          >
            {isVisible && (
              <View
                alignItems={ALIGN.CENTER}
                backgroundColor={isSelected ? color : undefined}
                justifyContent={ALIGN.CENTER}
                style={styles(
                  style.day,
                  isTouchable && style.focus,
                  isToday && (isDisabled ? style.todayDisabled : style.today),
                )}
              >
                <Text
                  color={isDisabled ? COLOR.LIGHTEN : isSelected ? COLOR.BASE : undefined}
                  level={2}
                  selectable={false}
                >
                  {toLocale(date, { day: 'numeric' })}
                </Text>
              </View>
            )}
          </Touchable>
        );
      })}
    </View>
  );
};

Week.displayName = 'Week';

Week.propTypes = {
  disabledDates: PropTypes.arrayOf(PropTypes.string),
  disabledPast: PropTypes.bool,
  disabledToday: PropTypes.bool,
  from: PropTypes.string,
  month: PropTypes.number.isRequired,
  number: PropTypes.number.isRequired,
  onPress: PropTypes.func,
  selected: PropTypes.any,
  to: PropTypes.string,
  year: PropTypes.number.isRequired,
};
