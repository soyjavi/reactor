import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';

import { Button } from '@atoms';
import { getToday, UTC } from '@helpers';
import { COLOR, FLEX_DIRECTION, SPACE, ALIGN } from '@hooks';
import { Text, View } from '@primitives';

import { DAYS, VISIBLE_WEEKS } from './Calendar.definition';
import { getFirstDateOfMonth, getHeader, getWeekDays, getWeekNumber } from './helpers';
import { Week } from './Week';

export const Calendar = ({
  color = COLOR.CONTENT,
  disabledDates = [],
  disabledPast = false,
  disabledToday = false,
  formatValue = 'YYYY/MM/DD',
  from,
  locale,
  to,
  value,
  onChange,
  ...others
}) => {
  const [instance, setInstance] = useState(getFirstDateOfMonth(getToday()));
  const [selected, setSelected] = useState(undefined);

  useEffect(() => {
    if (!value) return;
    setInstance(getFirstDateOfMonth(new Date(value)));
    setSelected(UTC(new Date(value)));
  }, [value]);

  const handleChange = (date) => {
    setSelected(date);
    onChange(date);
  };

  const handleMonth = (month) =>
    setInstance(getFirstDateOfMonth(new Date(instance.getFullYear(), instance.getMonth() + month)));

  const instanceTS = instance.getTime();
  const todayMonthTS = getFirstDateOfMonth(getToday()).getTime();

  const disabledPrevious =
    (disabledPast && instanceTS <= todayMonthTS) ||
    (from && instanceTS <= getFirstDateOfMonth(new Date(from)).getTime());
  const disabledNext = to && instanceTS >= getFirstDateOfMonth(new Date(to)).getTime();

  const weekNumber = getWeekNumber(instance);
  const weekdays = getWeekDays(locale);

  return (
    <View {...others}>
      <View
        flexDirection={FLEX_DIRECTION.ROW}
        alignItems={ALIGN.CENTER}
        paddingHorizontal={SPACE.XS}
        marginBottom={SPACE.L}
      >
        <Button disabled={disabledPrevious} icon="left" small squared onPress={() => handleMonth(-1)} />
        <Text action align={ALIGN.CENTER} flex={SPACE.XS} upperCase marginHorizontal={SPACE.XS}>
          {getHeader(instance, locale)}
        </Text>
        <Button disabled={disabledNext} icon="right" small squared onPress={() => handleMonth(1)} />
      </View>

      <View flexDirection={FLEX_DIRECTION.ROW} marginBottom={SPACE.S}>
        {DAYS.map((day) => (
          <Text action align={ALIGN.CENTER} flex={SPACE.XS} key={day} paddingHorizontal={SPACE.XS} upperCase>
            {weekdays[day]}
          </Text>
        ))}
      </View>

      {VISIBLE_WEEKS.map((week) => (
        <Week
          {...others}
          {...{ disabledDates, disabledPast, disabledToday, formatValue, from, to, selected }}
          color={color}
          key={week}
          month={instance.getMonth()}
          number={weekNumber + week}
          year={instance.getFullYear()}
          onPress={handleChange}
        />
      ))}
    </View>
  );
};

Calendar.displayName = 'Calendar';

Calendar.propTypes = {
  color: PropTypes.oneOf(Object.values(COLOR)),
  disabledDates: PropTypes.arrayOf(PropTypes.string),
  disabledPast: PropTypes.bool,
  disabledToday: PropTypes.bool,
  formatValue: PropTypes.string,
  from: PropTypes.string,
  locale: PropTypes.string,
  to: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.string,
};
