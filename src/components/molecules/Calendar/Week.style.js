import StyleSheet from 'react-native-extended-stylesheet';

export const style = StyleSheet.create({
  day: {
    borderRadius: '$spaceXL / 2',
    width: '$spaceXL',
    height: '$spaceXL',
  },

  focus: undefined,

  today: {
    borderWidth: '$borderSize',
    borderColor: '$colorContent',
  },

  todayDisabled: {
    borderWidth: '$borderSize',
    borderColor: '$colorLighten',
  },
});
