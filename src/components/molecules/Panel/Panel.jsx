import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useState } from 'react';

import { Overlay } from '@atoms';
import { ALIGN, COLOR, POINTER, POSITION, SIZE, styles, useBack, useBanStylerProps, useDevice, useSwipe } from '@hooks';
import { Motion, SafeAreaView, View } from '@primitives';

import { style } from './Panel.style';

const DELAY_MS = 200;

export const Panel = React.forwardRef(
  (
    {
      children,
      color = COLOR.INFO,
      hasOverlay = false,
      isVisible = false,
      align = ALIGN.LEFT,
      onClose,
      onHardwareBackPress,
      ...others
    },
    ref,
  ) => {
    const { screen } = useDevice();
    const handleSwipe = useSwipe({
      onSwiping: ({ deltaX, left, right } = {}) => {
        if ((!left && align === ALIGN.LEFT) || (!right && align === ALIGN.RIGHT)) return;
        setLeft(parseInt(deltaX, 10));
        setSwiping(true);
      },
      onSwiped: ({ deltaX, left, right } = {}) => {
        if ((!left && align === ALIGN.LEFT) || (!right && align === ALIGN.RIGHT)) return;

        setLeft(0);
        if (onClose && Math.abs(deltaX) >= screen.width / 3) onClose();
        else setTimeout(() => setSwiping(false), DELAY_MS);
      },
    });

    useBack(
      useCallback(() => {
        if (isVisible) {
          if (onHardwareBackPress) onHardwareBackPress();
          else if (onClose) onClose();
        }

        return isVisible;
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, [isVisible, onClose]),
    );

    const [left, setLeft] = useState(0);
    const [swiping, setSwiping] = useState(false);

    useEffect(() => {
      setSwiping(false);
      if (isVisible) setLeft(0);
    }, [isVisible]);

    return (
      <View
        {...(!isVisible
          ? {
              accessibilityElementsHidden: true,
              pointer: POINTER.NONE,
              importantForAccessibility: 'no-hide-descendants',
            }
          : undefined)}
        layer={SIZE.L}
        position={POSITION.FIXED}
        style={styles(
          style.panel,
          align === ALIGN.RIGHT && style.panelRight,
          hasOverlay ? (align === ALIGN.RIGHT ? style.overlayRight : style.overlay) : undefined,
          others.style,
          others.customStyle,
        )}
        role="aside"
        wide
      >
        {hasOverlay && <Overlay isVisible={isVisible} onPress={onClose} />}

        <Motion
          {...useBanStylerProps(others)}
          backgroundColor={color}
          disabled={left !== 0}
          layer={SIZE.L}
          position={POSITION.RELATIVE}
          ref={ref}
          role="section"
          style={style.motion}
          timing={screen.S && swiping && isVisible ? 'spring' : undefined}
          type={isVisible ? 'expand' : 'collapse'}
          value={{ translateX: isVisible ? left || '0%' : `${align === ALIGN.LEFT ? '-' : ''}100%` }}
        >
          {screen.S && (
            <View
              {...(isVisible ? handleSwipe : undefined)}
              layer={SIZE.XS}
              position={POSITION.ABSOLUTE}
              style={styles(style.swiper, align === ALIGN.RIGHT && style.swiperRight)}
            />
          )}
          <SafeAreaView style={style.safeAreaView}>{children}</SafeAreaView>
        </Motion>
      </View>
    );
  },
);

Panel.displayName = 'Panel';

Panel.propTypes = {
  align: PropTypes.oneOf([ALIGN.LEFT, ALIGN.RIGHT]),
  children: PropTypes.node.isRequired,
  color: PropTypes.string,
  hasOverlay: PropTypes.bool,
  isVisible: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
  onHardwareBackPress: PropTypes.func,
};
