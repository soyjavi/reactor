import StyleSheet from 'react-native-extended-stylesheet';

export const style = StyleSheet.create({
  motion: {
    height: '100%',
    overflow: 'scroll',
    width: '100%',
  },

  overlay: {
    paddingRight: `$spaceXXXL`,
  },

  overlayRight: {
    paddingLeft: `$spaceXXXL`,
  },

  panel: {
    left: 0,
    height: '100%',
    top: 0,
  },

  panelRight: {
    alignItems: 'flex-end',
  },

  safeAreaView: {
    flex: 1,
  },

  swiper: {
    bottom: '$layoutXXL',
    right: 0,
    top: '$layoutXXL',
    width: '$spaceXL',
  },

  swiperRight: {
    right: null,
    left: 0,
  },
});
