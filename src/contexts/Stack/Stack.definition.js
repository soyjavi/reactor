import { IS_JEST } from '@helpers';

const emptyFn = () => {
  if (!IS_JEST)
    // eslint-disable-next-line no-console
    console.warn("WARN: Can't use the hook useStack without a instance of <StackProvider>");
};

const DEFAULT_STACK = {
  show: emptyFn,
  alert: emptyFn,
  info: emptyFn,
  success: emptyFn,
  hide: emptyFn,
  wipe: emptyFn,
};

export { DEFAULT_STACK };
