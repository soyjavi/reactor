const STACK_REDUCER = {
  MOUNT: 'mount',
  SHOW: 'show',
  HIDE: 'hide',
  UNMOUNT: 'unmount',
  UNMOUNT_ALL: 'unmount_all',
};

const StackReducer = (state = {}, action = {}) => {
  const { type, id, component, props } = action;

  switch (type) {
    case STACK_REDUCER.MOUNT:
      return { ...state, [id]: { component: component, props: { ...props, isVisible: false } } };
    case STACK_REDUCER.SHOW:
      return { ...state, [id]: { ...state[id], props: { ...state[id].props, isVisible: true } } };
    case STACK_REDUCER.HIDE:
      return state[id] ? { ...state, [id]: { ...state[id], props: { ...state[id].props, isVisible: false } } } : state;
    case STACK_REDUCER.UNMOUNT:
      delete state[id];
      return { ...state };
    case STACK_REDUCER.UNMOUNT_ALL:
      return {};
    default:
      throw new Error(`Unhandled action type: ${type}`);
  }
};

export { STACK_REDUCER, StackReducer };
