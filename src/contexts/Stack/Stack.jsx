import PropTypes from 'prop-types';
import React, { useContext, useReducer } from 'react';

import { COLOR, POSITION, SPACE, styles, useDevice } from '@hooks';
import { View } from '@primitives';
import { Theme } from '@theming';

import { DEFAULT_STACK } from './Stack.definition';
import { STACK_REDUCER, StackReducer } from './Stack.reducer';
import { style } from './Stack.style';

const StackContext = React.createContext(DEFAULT_STACK);

const useStack = () => useContext(StackContext);

const StackProvider = ({ children, ...others }) => {
  const { screen } = useDevice();
  const [state, dispatch] = useReducer(StackReducer, {});

  const { motionExpand } = Theme.get();

  const value = {
    show: (id, component, props) => {
      dispatch({ type: STACK_REDUCER.MOUNT, component: component, id: id, props: props });
      setTimeout(() => {
        dispatch({ type: STACK_REDUCER.SHOW, id: id });
      }, 0);
    },
    alert: (id, component, props) => {
      value.show(id, component, { ...props, color: COLOR.ERROR });
    },
    info: (id, component, props) => {
      value.show(id, component, { ...props, color: COLOR.LIGHTEN });
    },
    success: (id, component, props) => {
      value.show(id, component, { ...props, color: COLOR.SUCCESS });
    },
    hide: (id) => {
      dispatch({ type: STACK_REDUCER.HIDE, id: id });
      setTimeout(() => {
        dispatch({ type: STACK_REDUCER.UNMOUNT, id: id });
      }, motionExpand);
    },
    wipe: () => {
      dispatch({ type: STACK_REDUCER.UNMOUNT_ALL });
    },
  };

  return (
    <StackContext.Provider value={value}>
      <View
        layer={SPACE.XL}
        position={POSITION.FIXED}
        style={styles(style.stack, screen.L && style.maxWidth, others.style)}
      >
        {Object.keys(state).map((key, index) => {
          const { component, props } = state[key];
          // eslint-disable-next-line react/prop-types
          const { onClose } = props;

          return React.createElement(component, {
            ...props,
            key,
            zIndex: index,
            onClose: () => {
              value.hide(key);

              onClose && onClose();
            },
          });
        })}
      </View>
      {children}
    </StackContext.Provider>
  );
};

StackProvider.displayName = 'ReactorStackProvider';

StackProvider.propTypes = {
  children: PropTypes.node,
};

export { useStack, StackContext, StackProvider };
