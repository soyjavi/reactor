import { Dimensions, Platform } from 'react-native';

import { IS_JEST } from '@helpers';

import { getBrowser, getScreen, getTouch } from './helpers';

const browser = getBrowser();
const touch = getTouch();
const native = Platform.OS === 'ios' || Platform.OS === 'android';
const web = Platform.OS === 'web';
const { height = 667, width = 375 } = IS_JEST ? {} : Dimensions.get('window');

const DEFAULT_RULERS = {
  S: [undefined, 599],
  M: [600, 839],
  L: [840, undefined],
};

const DEFAULT_DEVICE = {
  setRulers: () => {},
  screen: (() => {
    return getScreen(height, width, DEFAULT_RULERS);
  })(),
  platform: {
    ios: Platform.OS === 'ios',
    android: Platform.OS === 'android',
    web,
    native,
    server: !native && !web,
  },
  browser,
  isPortrait: height >= width,
  isLandscape: width > height,
  touch,
};

export { DEFAULT_DEVICE, DEFAULT_RULERS };
