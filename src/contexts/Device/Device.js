import PropTypes from 'prop-types';
import React, { useContext, useState } from 'react';
import { useWindowDimensions } from 'react-native';

import { DEFAULT_DEVICE, DEFAULT_RULERS } from './Device.definition';
import { getScreen } from './helpers';

const DeviceContext = React.createContext(DEFAULT_DEVICE);

const useDevice = () => useContext(DeviceContext);

const DeviceProvider = ({ children, forceViewport = {} }) => {
  const windowDimensions = useWindowDimensions();

  const [rulers, setRulers] = useState(DEFAULT_RULERS);

  const { height, width } =
    typeof forceViewport.height === 'number' && typeof forceViewport.width === 'number'
      ? forceViewport
      : windowDimensions;

  return (
    <DeviceContext.Provider
      value={{
        ...DEFAULT_DEVICE,
        setRulers: (values = {}) => setRulers({ ...DEFAULT_RULERS, ...values }),
        screen: getScreen(height, width, rulers),
        isPortrait: height >= width,
        isLandscape: width > height,
      }}
    >
      {children}
    </DeviceContext.Provider>
  );
};

DeviceProvider.propTypes = {
  children: PropTypes.node,
  forceViewport: PropTypes.shape({
    height: PropTypes.number,
    width: PropTypes.number,
  }),
};

export { DeviceContext, DeviceProvider, useDevice };
