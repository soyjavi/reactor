import { DEFAULT_RULERS } from '../useDevice.definition';

export const setRulers = (values = {}) => setRulers({ ...DEFAULT_RULERS, ...values });
