export const getBrowser = () => ({
  chrome: false,
  firefox: false,
  inApp: false,
  mobile: false,
  safari: false,
});
