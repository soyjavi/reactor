// TODO: We need test in web environment
import { getBrowser } from '..';

const setUserAgent = (value) => {
  navigator.__defineGetter__('userAgent', () => {
    return value;
  });
};

const CHROME =
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/540.36 (KHTML, like Gecko) Chrome/82.5.0005.77 Safari/540.36';
const FIREFOX = 'Mozilla/5.0 (X11; Linux i686; rv:64.0) Gecko/20100101 Firefox/64.0';
const SAFARI =
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.5 Safari/605.1.15';
const CHROME_MOBILE =
  'Mozilla/5.0 (Linux; Android 10; Pixel 3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.111 Mobile Safari/537.36';
const SAFARI_MOBILE =
  'Mozilla/5.0 (iPhone; CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0 Mobile/15E148 Safari/604.1';
const FB_IAB_ANDROID =
  'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/43.0.2357.121 Mobile Safari/537.36 [FB_IAB/FB4A;FBAV/35.0.0.48.273;]';
const FB_IAB_IOS =
  'Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 [FBAN/FBIOS;FBDV/iPhone12,1;FBMD/iPhone;FBSN/iOS;FBSV/13.5.1;FBSS/2;FBID/phone;FBLC/en_US;FBOP/5]';
const PINTEREST =
  'Mozilla/5.0 (Linux; Android 9; STF-L09 Build/HUAWEISTF-L09; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/79.0.3945.79 Mobile Safari/537.36 [Pinterest/Android]';

describe('@hooks/useStyler/styles', () => {
  test('default', () => {
    setUserAgent('');
    expect(getBrowser()).toEqual({ chrome: false, firefox: false, safari: false, mobile: false, inApp: false });
  });

  test('is chrome', () => {
    setUserAgent(CHROME);
    expect(getBrowser()).toEqual({ chrome: true, firefox: false, safari: false, mobile: false, inApp: false });
  });

  test('is firefox', () => {
    setUserAgent(FIREFOX);
    expect(getBrowser()).toEqual({ chrome: false, firefox: true, safari: false, mobile: false, inApp: false });
  });

  test('is safari', () => {
    setUserAgent(SAFARI);
    expect(getBrowser()).toEqual({ chrome: false, firefox: false, safari: true, mobile: false, inApp: false });
  });

  test('is a mobile browser', () => {
    setUserAgent(CHROME_MOBILE);
    expect(getBrowser()).toEqual({ chrome: true, firefox: false, safari: false, mobile: true, inApp: false });

    setUserAgent(SAFARI_MOBILE);
    expect(getBrowser()).toEqual({ chrome: false, firefox: false, safari: true, mobile: true, inApp: false });
  });

  test('is facebook in app browser for Android', () => {
    setUserAgent(FB_IAB_ANDROID);
    expect(getBrowser()).toEqual({ chrome: true, firefox: false, safari: false, mobile: true, inApp: true });
  });

  test('is facebook in app browser for IOS', () => {
    setUserAgent(FB_IAB_IOS);
    expect(getBrowser()).toEqual({ chrome: false, firefox: false, safari: false, mobile: true, inApp: true });
  });

  test('is Pinterest browser', () => {
    setUserAgent(PINTEREST);
    expect(getBrowser()).toEqual({ chrome: true, firefox: false, safari: false, mobile: true, inApp: true });
  });
});
