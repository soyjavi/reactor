// TODO: We need test in web environment
import { getTouch } from '../..';

const setUserAgent = (value) => {
  navigator.__defineGetter__('userAgent', () => {
    return value;
  });
};

const CHROME =
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36';
const CHROME_MOBILE =
  'Mozilla/5.0 (Linux; Android 6.0.1; RedMi Note 5 Build/RB3N5C; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/68.0.3440.91 Mobile Safari/537.36';
const SAFARI =
  'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6; en-en) AppleWebKit/533.19.4 (KHTML, like Gecko) Version/5.0.3 Safari/533.19.4';
const SAFARI_MOBILE =
  'Mozilla/5.0 (iPhone; CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1 Mobile/15E148 Safari/604.1';

describe('@hooks/useStyler/styles', () => {
  test('default', () => {
    setUserAgent('');
    expect(getTouch()).toEqual(false);
  });

  test('is chrome (desktop) browser', () => {
    setUserAgent(CHROME);
    expect(getTouch()).toEqual(false);
  });

  test('is chrome (mobile) browser', () => {
    setUserAgent(CHROME_MOBILE);
    expect(getTouch()).toEqual(true);
  });

  test('is safari (desktop) browser', () => {
    setUserAgent(SAFARI);
    expect(getTouch()).toEqual(false);
  });

  test('is safari (mobile) browser', () => {
    setUserAgent(SAFARI_MOBILE);
    expect(getTouch()).toEqual(true);
  });
});
