import PropTypes from 'prop-types';
import React, { useContext, useLayoutEffect, useMemo, useReducer } from 'react';

import { DEFAULT_PORTAL } from './Portal.definition';
import { PORTAL_REDUCER_TYPE, PortalReducer } from './Portal.reducer';

const PortalContext = React.createContext(DEFAULT_PORTAL);

const usePortal = () => useContext(PortalContext);

const PortalProvider = React.memo(({ children }) => {
  const [state, dispatch] = useReducer(PortalReducer, []);

  const busy = Object.values(state).filter(({ props: { isVisible = false } = {} } = {}) => isVisible).length > 0;

  const value = useMemo(
    () => ({
      busy,
      instance: true,
      mount: (id, component) => {
        dispatch({ type: PORTAL_REDUCER_TYPE.MOUNT, component, id });
      },
      unmount: (id) => {
        dispatch({ type: PORTAL_REDUCER_TYPE.UNMOUNT, id });
      },
    }),
    [busy],
  );

  return (
    <PortalContext.Provider value={value}>
      {Object.keys(state)
        .filter((key) => state[key] !== undefined)
        .map((key) => state[key])}
      {children}
    </PortalContext.Provider>
  );
});

PortalProvider.displayName = 'ReactorPortalProvider';

PortalProvider.propTypes = {
  children: PropTypes.node,
};

const Portal = React.memo(({ children, id }) => {
  const portal = usePortal();

  useLayoutEffect(() => {
    portal.mount(id, children);

    return () => portal.unmount(id);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [children, id]);

  return !portal.instance ? children : undefined;
});

Portal.displayName = 'ReactorPortal';

Portal.propTypes = {
  children: PropTypes.node.isRequired,
  id: PropTypes.string.isRequired,
};

export { Portal, PortalContext, PortalProvider, usePortal };
