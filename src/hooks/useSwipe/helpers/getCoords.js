export const getCoords = ({ moveX = 0, moveY = 0 } = {}) => ({
  X: parseInt(moveX, 10),
  Y: parseInt(moveY, 10),
});
