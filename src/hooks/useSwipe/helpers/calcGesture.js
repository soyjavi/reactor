import { getCoords } from './getCoords';

export const calcGesture = (gesture, event) => {
  const { startX, startY } = gesture;
  const { X: endX, Y: endY } = getCoords(event);

  return {
    ...gesture,
    up: startY > endY,
    down: startY < endY,
    left: startX > endX,
    right: startX < endX,
    deltaX: endX - Number(startX),
    deltaY: endY - Number(startY),
    endX: endX,
    endY: endY,
  };
};
