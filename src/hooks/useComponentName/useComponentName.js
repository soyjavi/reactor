import { useMemo } from 'react';

const camelCaseToDash = (myStr) => {
  return myStr.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
};

const useComponentName = (name) => useMemo(() => camelCaseToDash(name), [name]);

export { useComponentName };
