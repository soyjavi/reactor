import PropTypes from 'prop-types';

const DOMAINS = [
  'alignContent',
  'alignItems',
  'alignSelf',
  'backgroundColor',
  'borderColor',
  'color',
  'display',
  'flex',
  'flexDirection',
  'flexWrap',
  'justifyContent',
  'layer',
  'margin',
  'marginHorizontal',
  'marginVertical',
  'marginTop',
  'marginRight',
  'marginBottom',
  'marginLeft',
  'padding',
  'paddingHorizontal',
  'paddingVertical',
  'paddingTop',
  'paddingRight',
  'paddingBottom',
  'paddingLeft',
  'pointer',
  'position',
  'upperCase',
  'textAlign',
  'wide',
];

const DOMAIN_SHORTCUT = {
  backgroundColor: 'bg',
  borderColor: 'bc',
  zIndex: 'z',
};

const ALIGN = {
  AROUND: 'around',
  AUTO: 'auto',
  BETWEEN: 'between',
  STRETCH: 'stretch',
  START: 'start',
  CENTER: 'center',
  END: 'end',
  TOP: 'top',
  RIGHT: 'right',
  BOTTOM: 'bottom',
  LEFT: 'left',
};

const COLOR = {
  BASE: 'base',
  CONTENT: 'content',
  PRIMARY: 'primary',
  SECONDARY: 'secondary',
  LIGHTEN: 'lighten',
  DISABLED: 'disabled',
  ERROR: 'error',
  WARNING: 'warning',
  SUCCESS: 'success',
  OVERLAY: 'overlay',
  TOUCHABLE: 'touchable',
};

const COORD = {
  TOP: 'Top',
  RIGHT: 'Right',
  BOTTOM: 'Bottom',
  LEFT: 'Left',
};

const COORDS = [COORD.TOP, COORD.RIGHT, COORD.BOTTOM, COORD.LEFT];

const DISPLAY = {
  BLOCK: 'block',
  INLINE: 'inline',
  INLINE_BLOCK: 'inlineBlock',
  FLEX: 'flex',
  NONE: 'none',
};

const FLEX_DIRECTION = {
  COLUMN: 'column',
  ROW: 'row',
};

const FLEX_WRAP = {
  WRAP: 'wrap',
};

const FONT = {
  HEADLINE: 'headline',
  PARAGRAPH: 'paragraph',
  ACTION: 'action',
  SMALL: 'small',
};

const POINTER = {
  ALL: 'all',
  AUTO: 'auto',
  NONE: 'none',
};

const POSITION = {
  ABSOLUTE: 'absolute',
  FIXED: 'fixed',
  RELATIVE: 'relative',
  STICKY: 'sticky',
};

const SPACE = {
  XXXS: 'XXXS',
  XXS: 'XXS',
  XS: 'XS',
  S: 'S',
  M: 'M',
  L: 'L',
  XL: 'XL',
  XXL: 'XXL',
  XXXL: 'XXXL',
  RESET: '0',
};

const SUFFIXES = {
  Horizontal: [COORD.RIGHT, COORD.LEFT],
  Vertical: [COORD.TOP, COORD.BOTTOM],
};

const StylerPropTypes = {
  alignContent: PropTypes.oneOf(Object.values(ALIGN)),
  alignItems: PropTypes.oneOf(Object.values(ALIGN)),
  alignSelf: PropTypes.oneOf(Object.values(ALIGN)),
  backgroundColor: PropTypes.oneOf(Object.values(COLOR)),
  borderColor: PropTypes.oneOf(Object.values(COLOR)),
  color: PropTypes.oneOf(Object.values(COLOR)),
  display: PropTypes.oneOf(Object.values(DISPLAY)),
  flexDirection: PropTypes.oneOf(Object.values(FLEX_DIRECTION)),
  justifyContent: PropTypes.oneOf(Object.values(ALIGN)),
  layer: PropTypes.oneOf(Object.values(SPACE)),
  margin: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({}), PropTypes.arrayOf(PropTypes.string)]),
  marginHorizontal: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
  marginVertical: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
  marginTop: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
  marginRight: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
  marginBottom: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
  padding: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({}), PropTypes.arrayOf(PropTypes.string)]),
  paddingHorizontal: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
  paddingVertical: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
  paddingTop: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
  paddingRight: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
  paddingBottom: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
  paddingLeft: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
  position: PropTypes.oneOf(Object.values(POSITION)),
  textAlign: PropTypes.oneOf(Object.values(ALIGN)),
  upperCase: PropTypes.bool,
  wide: PropTypes.bool,
};

export {
  DOMAINS,
  DOMAIN_SHORTCUT,
  ALIGN,
  COLOR,
  COORD,
  COORDS,
  DISPLAY,
  FLEX_DIRECTION,
  FLEX_WRAP,
  FONT,
  POINTER,
  POSITION,
  SPACE,
  SUFFIXES,
  StylerPropTypes,
};
