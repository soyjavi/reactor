import { StyleSheet } from 'react-native';

import { useBanProps } from '../useBanProps';
import { styler } from './styler';
import styles from './useStyler.style';

export const useStyler = ({ style, ...props } = {}, displayName, device = {}) => {
  const values = styler(useBanProps(props, ['pointer']), device.screen).map((key) => styles[key]);

  if (style) values.push(...(Array.isArray(style) ? style : [style]));

  const stylesheets = values.filter((value) => typeof value !== 'object');

  const objectStyle = StyleSheet.flatten(values.filter((value) => typeof value === 'object'));
  if (Object.keys(objectStyle).length > 0) stylesheets.push(objectStyle);

  return {
    style: stylesheets,
  };
};
