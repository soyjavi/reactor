// TODO: This is not a real "hook"
export const useBanProps = (props = {}, bannedProps = []) => {
  const safeProps = {};

  Object.keys(props).forEach((prop) => {
    if (!bannedProps.includes(prop)) safeProps[prop] = props[prop];
  });

  return safeProps;
};
