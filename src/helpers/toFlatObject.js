export const toFlatObject = (value = {}) =>
  Array.isArray(value)
    ? value.reduce(
        (res, currentValue) =>
          Array.isArray(currentValue) ? { ...res, ...toFlatObject(currentValue) } : { ...res, ...currentValue },
        {},
      )
    : value;
