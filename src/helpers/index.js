// CONST
export * from './KEYBOARD';

// Methods
export * from './capitalize';
export * from './getInputErrors';
export * from './getToday';
export * from './isJest';
export * from './isValidDate';
export * from './isValidEmail';
export * from './isValidPhone';
export * from './testID';
export * from './toAccesibilityRole';
export * from './toLocale';
export * from './toFlatObject';
export * from './UTC';

// Styling
export * from './stylesheetColor';
export * from './stylesheetFont';
