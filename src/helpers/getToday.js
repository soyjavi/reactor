import { UTC } from './UTC';

export const getToday = () => UTC(new Date());
