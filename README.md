# 👩🏻 aurora-design-system

> A clean & isomorphic design system from Lookiero Tech.

## 📦 Setup

Add the package in your project:

```
yarn add @soyjavi/reactor
```

## 🗺 Dependencies Map

<img src='https://i.imgur.com/qasZdqc.png' height='512' />

## Components (62)

- **Primitives** (14)

  - [Reactor](https://soyjavi.com/reactor/storyteller/#1)
  - [Icon](https://soyjavi.com/reactor/storyteller/#3)
  - [Image](https://soyjavi.com/reactor/storyteller/#4)
  - [Input](https://soyjavi.com/reactor/storyteller/#5)
  - [InputFile](https://soyjavi.com/reactor/storyteller/#6)
  - [Motion](https://soyjavi.com/reactor/storyteller/#7)
  - [Scrollview](https://soyjavi.com/reactor/storyteller/#9)
  - [Switch](https://soyjavi.com/reactor/storyteller/#10)
  - [Select](https://soyjavi.com/reactor/storyteller/#11)
  - [Text](https://soyjavi.com/reactor/storyteller/#12)
  - [Touchable](https://soyjavi.com/reactor/storyteller/#13)
  - [View](https://soyjavi.com/reactor/storyteller/#14)

  - **Atoms** (27)
    - [Box](https://soyjavi.com/reactor/storyteller/#17)
    - [Button](https://soyjavi.com/reactor/storyteller/#18)
    - [Form](https://soyjavi.com/reactor/storyteller/#21)
    - [InputField](https://soyjavi.com/reactor/storyteller/#22)
      - [variation:date](https://soyjavi.com/reactor/storyteller/#23)
      - [variation:fieldset](https://soyjavi.com/reactor/storyteller/#24)
      - [variation:phone](https://soyjavi.com/reactor/storyteller/#25)
      - [variation:select](https://soyjavi.com/reactor/storyteller/#26)
    - [InputOption](https://soyjavi.com/reactor/storyteller/#27)
    - [Layout](https://soyjavi.com/reactor/storyteller/#28)
    - [Link](https://soyjavi.com/reactor/storyteller/#29)
    - [Overlay](https://soyjavi.com/reactor/storyteller/#33)
    - [Progress](https://soyjavi.com/reactor/storyteller/#34)
    - [Rating](https://soyjavi.com/reactor/storyteller/#35)
    - [Skeleton](https://soyjavi.com/reactor/storyteller/#36)
    - [Slider](https://soyjavi.com/reactor/storyteller/#37)
    - [Table](https://soyjavi.com/reactor/storyteller/#40)
    - [Thumbnail](https://soyjavi.com/reactor/storyteller/#41)
    - [Wheelpicker](https://soyjavi.com/reactor/storyteller/#42)
    - **Molecules** (9)
      - [Calendar](https://soyjavi.com/reactor/storyteller/#43)
      - [Footer](https://soyjavi.com/reactor/storyteller/#44)
      - [Header](https://soyjavi.com/reactor/storyteller/#45)
      - [InputImage](https://soyjavi.com/reactor/storyteller/#46)
      - [Modal](https://soyjavi.com/reactor/storyteller/#47)
      - [Notification](https://soyjavi.com/reactor/storyteller/#48)
      - [OptionImage](https://soyjavi.com/reactor/storyteller/#49)
      - [Panel](https://soyjavi.com/reactor/storyteller/#50)
      - [Tooltip](https://soyjavi.com/reactor/storyteller/#51)

### Contexts (3)

- [Device](https://soyjavi.com/reactor/storyteller/#54)
- [Portal](https://soyjavi.com/reactor/storyteller/#55)
- [Stack](https://soyjavi.com/reactor/storyteller/#56)

### Hooks (2)

- [useStyler](https://soyjavi.com/reactor/storyteller/#57)
- [useSwipe](https://soyjavi.com/reactor/storyteller/#58)

## Running the _Storyteller_

You have several ways to run Reactor's _Storyteller_. Perhaps the simplest is through the environment offered by `expo`, you should simply execute:

```bash
yarn start
```

With this, the default web environment will be started, but you can also open new instances of the _Storyteller_ on both iOS and Android.

In case you prefer to run the native environment `metro` you should do it through 2 steps:

```bash
yarn start: native
```

With this you start the `metro` environment and now you must select which system you want to run:

```bash
yarn start: native: ios
// or
yarn start: native: android
```
